#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  1 10:49:49 2017

@author: max
"""

import cv2
import numpy as np
from sklearn.externals import joblib
from skimage.feature import hog
from auto_canny import *
from matplotlib import pyplot as plt

class Digit_Finder:
    
    def __init__(self, grid, tailed_grid, debug=False):
        
        self.result = []
        #gray = cv2.cvtColor(grid, cv2.COLOR_BGR2GRAY)
        
        # load classifier
        knn = joblib.load('./digits_classificator.pkl')
        
        counter = 0
        for i in range(83, 164):
            
            tmp = tailed_grid.copy()
            
            #transform to mask
            tmp[tmp==i] = 255
            tmp[tmp!=255] = 0
            
            
            num = cv2.bitwise_and(grid, grid, mask=tmp.astype('uint8'))
            
            counter += 1
            num = self.recognize_num(knn, num, counter, tmp.astype('uint8'))
            
            self.result.append(int(num))
            print(str(num))
            
        # write to file
        np.savetxt("./recognized.txt", np.transpose(self.result), fmt='%01d', delimiter='', newline='')

          
        
        
    def recognize_num(self, classifier, img, counter, mask):
        
#        if(counter==69):
#            plt.figure(1100)
#            plt.imshow(img, cmap='gray')

        
        _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        x, y, w, h = cv2.boundingRect(contours[0])
        img = img[y:y+h,x:x+w]
        
        img_backup = img.copy()
        
        
        img = cv2.GaussianBlur(img, (5, 5), 0)
        _, img = cv2.threshold(img,105,255,cv2.THRESH_BINARY)
        img = 255-img
        
        img = cv2.erode(img, (5, 5), iterations=5)
        
        
        backup = None
        num_labels=1
        iterations = 0
        while(num_labels==1 and iterations<=10):
            iterations += 1
            
            print("erosion")
        
            backup = img.copy()
            
            
            img = cv2.erode(img, (5, 5), iterations=1)
            
            #erode until its needed
            output = cv2.connectedComponentsWithStats(img, 4, cv2.CV_32S)
            # The first cell is the number of labels
            # 0 are also labelled so: -1
            num_labels = (output[0]-1)
        
            # use last good result
        img = backup
        
        
        
        roi = cv2.resize(img, (28, 28), interpolation=cv2.INTER_AREA)
        roi = cv2.GaussianBlur(roi, (5, 5), 0)
        
        roi_backup = roi.copy()
        # estimate quantity of white pixels
        
        roi[roi!=0] = 1
        whiteness = int(np.sum(roi))
        wh_rate = round((whiteness / (w*h)), 3)
        print("Whiness"+str(counter)+": "+str(wh_rate))
        
        
        
#        if(counter==69):
#            plt.figure(111)
#            plt.imshow(roi_backup, cmap='gray')
            
        
        
        if(wh_rate < 0.003):
            return (0)
            
        else:
            
            # write each non empty cell
            dest_dir =  "./output_images/"+str(counter)+".png"
            roi_to_write = cv2.cvtColor(img_backup, cv2.COLOR_GRAY2RGB)
            cv2.imwrite(dest_dir, roi_to_write)
            
            
            # calculate HOG features
            roi_hog_fd = hog(roi_backup, orientations=9, pixels_per_cell=(14, 14), cells_per_block=(1, 1), visualise = False)
            nbr = classifier.predict(np.array([roi_hog_fd], 'float32'))
            
            return ( int(nbr[0]) )  
        
    
    