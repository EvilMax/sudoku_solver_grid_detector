#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 29 13:31:47 2016

@author: max
"""
import cv2
import math
import numpy as np


def resize_to_default(img, debug=False):
    
    # resize to default width, by keeping height ratio
    width = 1280
    ratio = float(width) / img.shape[1]
    dim = (width, int(img.shape[0] * ratio))

    # perform the actual resizing of the image and show it
    # INTER_AREA gives better result compared to LINEAR
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
        
    return (img)




def improve_obtained_grid_Hough(grid, tollerance=1000, line_length=1500, line_width=5):
        
    #fix lines
    lines= cv2.HoughLines(grid.copy(), 1, math.pi/180.0, tollerance, np.array([]), 0, 0)
    
    if(lines is None):
        print("Hough doesnt produce any effect with tollerance: "+str(tollerance))
        return grid
    
    a,b,c = lines.shape
    for i in range(a):
        rho = lines[i][0][0]
        theta = lines[i][0][1]
        a = math.cos(theta)
        b = math.sin(theta)
        x0, y0 = a*rho, b*rho
        pt1 = ( int(x0+line_length*(-b)), int(y0+line_length*(a)) )
        pt2 = ( int(x0-line_length*(-b)), int(y0-line_length*(a)) )
        cv2.line(grid, pt1, pt2, (255, 255, 255), line_width, cv2.LINE_AA)

    return (grid)
        
        
        
def apply_canny_treshold(img, sigma=0.33):

    # compute the median of the single channel pixel intensities
    v = np.median(img)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(img, lower, upper)

    # return the edged image
    return (edged)
        
        
        
def find_grid_cells(grid_mask):
    
    _, contours, _ = cv2.findContours(grid_mask.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    #create new array for future ordering
    grid_labels = np.zeros( grid_mask.shape, dtype='uint8' )
    
    #i will draw here
    result = cv2.cvtColor(grid_mask.copy(), cv2.COLOR_GRAY2RGB)
    
    
    width, height = grid_mask.shape

    counter = 0
    
    minArea = (width*height)/200
    maxArea = (width*height)/50
    
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if area > width:
                epsilon = 0.05 * cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, epsilon, True)
                
           
                #draw also rectangle to save time:
                if area > minArea and area < maxArea and len(approx)>=4 and len(approx)<=8:
                #if area > width and len(approx)>=4 and len(approx)<=8:
                  
                    
                    x, y, w, h = cv2.boundingRect(cnt)
                    
                    #take just only things similar to squares
                    ar = w / float(h)
                    
                    if(ar >= 0.85 and ar <= 1.2):
                        counter = counter + 1
                
                        cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)
                        
                        
                        # add also some text
                        # Write some Text
                        font = cv2.FONT_HERSHEY_SIMPLEX
                        cv2.putText(result, str(counter),(x+10,y+20), font, 2,(255,255,255),2)
                        
                        #fill correctly grid labels with white spaces
                        grid_labels[y:y+h, x:x+w] = 255
    
    #CONNECTED COMPONENTS LABELLING
    # You need to choose 4 or 8 for connectivity type
    connectivity = 4  
    # Perform the operation
    output = cv2.connectedComponentsWithStats(grid_labels.copy(), connectivity, cv2.CV_32S)
    # Get the results
    # The first cell is the number of labels
    num_labels = output[0]
    # The second cell is the label matrix
    labels = output[1]
    # The third cell is the stat matrix
    stats = output[2]
    # The fourth cell is the centroid matrix
    centroids = output[3]
    
    
    
    #better algorithm
    step=20
    
    need_change = True
    counter = num_labels+1
    first=True
    for i in range(0, width, step):
        #old_counter=counter
        need_change=True
        first=True
        for j in range(0, height, step):
            
            #debug, but breaks result
            #cv2.circle(labels,(i,j), 10, (0,0,0), -1)
            
            if(i>120 and j>j*step and first==True):
                cv2.circle(labels,(i,j), 10, (255,255,255), -1)
                need_change=False
                first=False
            
            if(labels[i][j] != 0 and need_change==True and not(labels[i][j]>=(num_labels+1) and labels[i][j]<=counter )):
                labels[labels==labels[i][j]] = counter
                counter += 1
                need_change = True
                first=False
                #cv2.circle(labels,(i,j), 10, (0,0,0), -1)
                
                
    return [labels, result, num_labels]