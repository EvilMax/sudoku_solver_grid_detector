import sys
from cx_Freeze import setup, Executable

#no Need of this to be imported manually, but just to remember about their existance
''' 'Core.py', 'Deeper_Grid_Analyzer.py', 'Shared_Functions.py', 'Digit_finder', 'Draw_solution.py', 'Learn.py', 'Grid_Detector.py', 'Loader.py' '''

includes = []
excludes = []
packages = ['numpy', 'scipy', 'matplotlib', 'tkinter', 'cv2', 'argparse', 'os', 'glob', 'colorama', 'datetime', 'sklearn', 'skimage']
path = []
base = None

if sys.platform == 'win32':
    base = 'Win32GUI'

options = {
    'build_exe': {
        "includes": includes,
        "excludes": excludes,
        "packages": packages,
        "path": path
        #'excludes': ['Tkinter']  # Sometimes a little finetuning is needed
    }
}

executables = [Executable('main.py', base=base)]
setup(name='nuitka',
      version='0.1',
      description='Sample PyQT5-matplotlib script',
      executables=executables,
      options=options
      )