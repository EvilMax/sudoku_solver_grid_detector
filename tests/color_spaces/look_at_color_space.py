#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 11:37:15 2016

@author: max
"""

import cv2
import matplotlib.pyplot as plt

img = cv2.imread("./../../images/first.jpg")
height, width = img[:, :, 0].shape

ratio = 0.25
new_height = round(ratio*height)
new_width = round(ratio*width)

# now resize for better performance
resized = cv2.resize(img, (new_width, new_height),
                     interpolation=cv2.INTER_AREA)

############################
# change color space to YCbCr
############################

ycrcb = cv2.cvtColor(resized, cv2.COLOR_BGR2YCR_CB)

plt.figure(1)

plt.subplot(311), plt.imshow(ycrcb[:, :, 0], cmap='gray'), plt.title("Y")
plt.axis("off")
plt.subplot(312), plt.imshow(ycrcb[:, :, 1], cmap='gray'), plt.title("Cr")
plt.axis("off")
plt.subplot(313), plt.imshow(ycrcb[:, :, 2], cmap='gray'), plt.title("Cb")
plt.axis("off")

############################
# change color space to HSV
############################

hsv = cv2.cvtColor(resized, cv2.COLOR_BGR2HSV)

plt.figure(2)

plt.subplot(311), plt.imshow(hsv[:, :, 0], cmap='gray'), plt.title("H")
plt.axis("off")
plt.subplot(312), plt.imshow(hsv[:, :, 1], cmap='gray'), plt.title("S")
plt.axis("off")
plt.subplot(313), plt.imshow(hsv[:, :, 2], cmap='gray'), plt.title("V")
plt.axis("off")

plt.show()
