#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 12:01:11 2016

@author: max
"""

import cv2
import matplotlib.pyplot as plt
import numpy as np


def auto_canny(image, sigma=0.33):
    # compute the median of the single channel pixel intensities
    v = np.median(image)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    # return the edged image
    return edged

#need to execute preprocessing for shadows
img = cv2.imread("./../../images/first.jpg")
height, width = img[:, :, 0].shape

ratio = 0.1
new_height = round(ratio*height)
new_width = round(ratio*width)

# now resize for better performance
resized = cv2.resize(img, (new_width, new_height),
                     interpolation=cv2.INTER_AREA)

############################
# change color space to YCbCr
############################

ycrcb = cv2.cvtColor(resized, cv2.COLOR_BGR2YCR_CB)
Y = ycrcb[:, :, 0]

edges = cv2.Canny(Y, 10, 20)
# best results from canny edge detection using sobel Kernel
edges_auto = auto_canny(Y)

plt.figure(1)
plt.subplot(311), plt.imshow(img[:, :, 0], cmap='gray'), plt.title('Otiginal')
plt.axis("off")
plt.subplot(312), plt.imshow(edges, cmap='gray'), plt.title('Edges found')
plt.axis("off")
plt.subplot(313), plt.imshow(edges_auto, cmap='gray'), plt.title('Edges found automatically')
plt.axis("off")


plt.figure(2)
plt.imshow(edges_auto, cmap='gray'), plt.title('Edges found automatically')
plt.axis("off")

##############################
# APPLY CLOSING / OPENING
##############################

# create kernel of ones
kernel = np.ones((3, 3), np.uint8)

opening = cv2.morphologyEx(edges_auto, cv2.MORPH_OPEN, kernel)
closing = cv2.morphologyEx(edges_auto, cv2.MORPH_CLOSE, kernel)

plt.figure(3)
plt.subplot(121), plt.imshow(opening, cmap='gray'), plt.title('OPENING')
plt.subplot(122), plt.imshow(closing, cmap='gray'), plt.title('CLOSING')

plt.figure(4)
plt.imshow(closing, cmap='gray'), plt.title('Closing')

# closing is best for now
_, contours, _ = cv2.findContours(closing.copy(), 1, 2)

result = cv2.cvtColor(closing.copy(), cv2.COLOR_GRAY2RGB)

counter = 0
for cnt in contours:

    x, y, w, h = cv2.boundingRect(cnt)

    epsilon = 0.02 * cv2.arcLength(cnt, True)
    approx = cv2.approxPolyDP(cnt, epsilon, True)
    #print(len(approx))

    if len(approx) == 4:

        counter = counter + 1
        print("Figure number: " + str(counter) + " Type: square")

        cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 1)

# already converted to RGB not BGR!
#result = cv2.cvtColor(result, cv2.COLOR_GRAY2RGB)

plt.figure(5)
plt.imshow(result), plt.title('Result')
plt.axis("off")
