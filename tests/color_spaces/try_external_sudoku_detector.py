#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 23 12:07:41 2016

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
from auto_canny import *

#img =  cv2.imread('./first.jpg')
img =  cv2.imread('./../../images/5.jpg')


#resize image to default size by keeping ratio
width = 1280
r = float(width) / img.shape[1]
height = int(img.shape[0] * r)
dim = (width, height)
 
# perform the actual resizing of the image and show it
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

ycrcb = cv2.cvtColor(resized, cv2.COLOR_BGR2YCR_CB)
Y = ycrcb[:, :, 0]

width, height = Y.shape

# adaptative histogramm gamma correction
# create a CLAHE object (Arguments are optional).
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
Y = clahe.apply(Y)

thresh = auto_canny(Y)

kernel = np.ones((3, 3), np.uint8)
dilated = cv2.dilate(thresh.copy(), kernel, iterations=1)

#dilate more if needed
tot_wh_pixels = round((np.count_nonzero(dilated == 255) )/(height*width), 2)
while tot_wh_pixels <= 0.2:
    
    dilated = cv2.dilate(dilated, kernel, iterations=1)
    
    tot_wh_pixels = round((np.count_nonzero(dilated == 255) )/(height*width), 2)
    print("Number of white pixels / whole img: "+ str(tot_wh_pixels))


#lines = cv2.HoughLines(dilated,1,np.pi/180,200)
#for rho,theta in lines[0]:
#    a = np.cos(theta)
#    b = np.sin(theta)
#    x0 = a*rho
#    y0 = b*rho
#    x1 = int(x0 + 1000*(-b))
#    y1 = int(y0 + 1000*(a))
#    x2 = int(x0 - 1000*(-b))
#    y2 = int(y0 - 1000*(a))
#
#    cv2.line(dilated,(x1,y1),(x2,y2),(255,255,255),2)
    
_, contours, _ = cv2.findContours(dilated.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

result = cv2.cvtColor(dilated.copy(), cv2.COLOR_GRAY2RGB)
#for cnt in contours:
#
#    x, y, w, h = cv2.boundingRect(cnt)
#
#    epsilon = 0.01 * cv2.arcLength(cnt, True)
#    approx = cv2.approxPolyDP(cnt, epsilon, True)
#    
#    area = cv2.contourArea(cnt)
#    
#    if area > width and len(approx)>=3.8 and len(approx)<=4.5:
#
#        cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)


biggest = None
max_area = 0
for cnt in contours:
        area = cv2.contourArea(cnt)
        if area > width:
                epsilon = 0.05 * cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, epsilon, True)
                
                
                
                #draw also rectangle to save time:
                if area > width and len(approx)>=3.8 and len(approx)<=4.5:
                    x, y, w, h = cv2.boundingRect(cnt)
                    cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)
                
                if area > max_area and len(approx)>=3.8 and len(approx)<=4.5:
                        biggest = approx
                        max_area = area

print(str(max_area))
x, y, w, h = cv2.boundingRect(biggest)
sudoku = np.zeros(Y.shape, dtype='uint8')


#print(str(x))

sudoku[y:y+h, x:x+w]=255

#find sudoku pixels and crop image
sudoku = cv2.bitwise_and(Y, Y, mask=sudoku)
sudoku = sudoku[y:y+h, x:x+w]

#resize image to default size by keeping ratio
width = 1280
r = float(width) / sudoku.shape[1]
dim = (width, int(sudoku.shape[0] * r))
 
# perform the actual resizing of the image and show it
sudoku = cv2.resize(sudoku, dim, interpolation = cv2.INTER_AREA)




                        
plt.figure(1)
plt.subplot(211), plt.imshow(Y, cmap='gray'), plt.title('Original')
plt.axis("off")
plt.subplot(212), plt.imshow(dilated, cmap='gray'), plt.title('Edges found automatically')
plt.axis("off")

plt.figure(2)
plt.imshow(result)

plt.figure(3)
plt.imshow(sudoku, cmap='gray')
