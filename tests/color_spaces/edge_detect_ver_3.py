#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 22 18:50:16 2016

@author: max
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 19:02:08 2016

@author: max
"""

import cv2
import matplotlib.pyplot as plt
import numpy as np


def auto_canny(image, sigma=0.33):
    # compute the median of the single channel pixel intensities
    v = np.median(image)
    
    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    # return the edged image
    return edged

    
#need to execute preprocessing for shadows
img = cv2.imread("./../../images/IMG_20161128_114847.jpg")

#from small to big
#img = cv2.imread("./first.jpg")

#resize image to default size by keeping ratio
width = 1280
r = float(width) / img.shape[1]
dim = (width, int(img.shape[0] * r))
 
# perform the actual resizing of the image and show it
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

############################
# change color space to YCbCr
############################

ycrcb = cv2.cvtColor(resized, cv2.COLOR_BGR2YCR_CB)
Y = ycrcb[:, :, 0]

#lets blurr image a bit
#f_size = 5
#sigma = ((f_size-1)/2)/(2.5)
#Y = cv2.GaussianBlur(Y, (f_size, f_size), sigma)

#Y = cv2.medianBlur(Y, 5)
#
##sharp image edges
#kernel_sharpen_3 = np.array([[-1,-1,-1,-1,-1],
#                             [-1,2,2,2,-1],
#                             [-1,2,8,2,-1],
#                             [-1,2,2,2,-1],
#                             [-1,-1,-1,-1,-1]]) / 8.0
#                             
#Y = cv2.filter2D(Y, -1, kernel_sharpen_3)

# best results from canny edge detection using sobel Kernel
edges_auto = auto_canny(Y)

kernel = np.ones((3, 3), np.uint8)
edges_auto = cv2.dilate(edges_auto, kernel, iterations=3)

kernel = np.ones((3, 3), np.uint8)
edges_auto = cv2.morphologyEx(edges_auto, cv2.MORPH_CLOSE, kernel)

plt.figure(1)
plt.subplot(211), plt.imshow(Y, cmap='gray'), plt.title('Original')
plt.axis("off")
plt.subplot(212), plt.imshow(edges_auto, cmap='gray'), plt.title('Edges found automatically')
plt.axis("off")


plt.figure(2)
plt.imshow(edges_auto, cmap='gray'), plt.title('Edges found automatically')
plt.axis("off")

##############################
# APPLY CLOSING / OPENING
##############################

# create kernel of ones
#kernel = np.ones((3, 3), np.uint8)

#closing = cv2.morphologyEx(edges_auto, cv2.MORPH_CLOSE, kernel)
#closing = cv2.erode(closing, kernel, iterations=1)



#closing = edges_auto
#
#plt.figure(4)
#plt.imshow(closing, cmap='gray'), plt.title('Closing')
#
## closing is best for now
#_, contours, _ = cv2.findContours(closing.copy(), 1, 2)
#
#result = cv2.cvtColor(closing.copy(), cv2.COLOR_GRAY2RGB)
#
#counter = 0
#for cnt in contours:
#
#    x, y, w, h = cv2.boundingRect(cnt)
#
#    epsilon = 0.03 * cv2.arcLength(cnt, True)
#    approx = cv2.approxPolyDP(cnt, epsilon, True)
#
#    if len(approx) == 4:
#
#        counter = counter + 1
#        print("Figure number: " + str(counter) + " Type: square")
#
#        cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)
#
#
#plt.figure(5)
#plt.imshow(result), plt.title('Result')
#plt.axis("off")

######
# lets play with hough transform
######



#
#result_hough = cv2.cvtColor(edges_auto.copy(), cv2.COLOR_GRAY2RGB)
#tmp = edges_auto.copy()
#
#minLineLength = 1
#maxLineGap = 1
#lines = cv2.HoughLines(tmp, 1, np.pi/180, 200)
#for rho,theta in lines[0]:
#    a = np.cos(theta)
#    b = np.sin(theta)
#    x0 = a*rho
#    y0 = b*rho
#    x1 = int(x0 + 1000*(-b))
#    y1 = int(y0 + 1000*(a))
#    x2 = int(x0 - 1000*(-b))
#    y2 = int(y0 - 1000*(a))
#
#    cv2.line(result_hough,(x1,y1),(x2,y2),(0,0,255),2)
#        
#plt.figure("Hough")
#plt.imshow(result_hough), plt.title('HOUGH')