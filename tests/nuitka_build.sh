#!/bin/bash

#export PYTHONPATH=$PYTHONPATH:"/usr/lib/python3/dist-packages/"
export PYTHONPATH="/usr/lib/python3/dist-packages/"

nuitka --standalone --clang --python-version=3.5 --python-flag=no_site --nofreeze-stdlib --show-progress ./nuitka.py