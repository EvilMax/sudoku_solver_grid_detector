#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 12:40:56 2016

@author: max
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt

img = cv2.imread('shapes.png')
gray = cv2.imread('shapes.png', 0)

hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

plt.figure(3)
plt.subplot(311), plt.imshow(hsv[:, :, 0], cmap='gray')
plt.axis("off")
plt.subplot(312), plt.imshow(hsv[:, :, 1], cmap='gray')
plt.axis("off")
plt.subplot(313), plt.imshow(hsv[:, :, 2], cmap='gray')
plt.axis("off")

# best color space
S = hsv[:, :, 1]

ret, thresh = cv2.threshold(S, 127, 255, cv2.THRESH_BINARY)
#thresh = cv2.adaptiveThreshold(gray, 255,
#                               cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
#                               cv2.THRESH_BINARY, 11, 2)
#ret, thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

plt.figure(1)
plt.subplot(211), plt.imshow(gray, cmap='gray')
plt.axis("off")
plt.subplot(212), plt.imshow(thresh, cmap='gray')
plt.axis("off")

_, contours, _ = cv2.findContours(thresh, 1, 2)

counter = 0
for cnt in contours:
    
    x,y,w,h = cv2.boundingRect(cnt)
    
    counter = counter + 1
    print("Figure: " + str(counter))
    
    approx = cv2.approxPolyDP(cnt, 0.01*cv2.arcLength(cnt, True), True)
    print(len(approx))
    if len(approx) == 5:
        print("pentagon")
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
    elif len(approx) == 3:
        print("triangle")
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
    elif len(approx) == 4:
        print("square")
        cv2.rectangle(img,(x,y),(x+w,y+h),(55,55,89),2)
    elif len(approx) == 9:
        print("half-circle")
        cv2.rectangle(img,(x,y),(x+w,y+h),(150,255,150),2)
    elif len(approx) > 15:
        print("circle")
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,175,190),2)

plt.figure(2)
plt.imshow(img, cmap='gray'), plt.title('Result')
plt.axis("off")
