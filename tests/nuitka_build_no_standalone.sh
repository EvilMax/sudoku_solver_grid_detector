#!/bin/bash

export PYTHONPATH="/usr/lib/python3/dist-packages/"
#export PYTHONPATH="/usr/lib/python3/dist-packages/:/usr/local/lib/python3.5/dist-packages/"

nuitka-run --standalone --recurse-to=numpy.core.arrayprint --python-version=3.5 --python-flag=no_site --nofreeze-stdlib ./nuitka.py