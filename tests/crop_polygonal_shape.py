#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 29 12:14:18 2016

@author: max
"""

import cv2
import numpy as np

# original image
image = cv2.imread('./../images/8a.jpg')

#resize image to default size by keeping ratio
width = 1280
r = float(width) / image.shape[1]
height = int(image.shape[0] * r)
dim = (width, height)
 
# perform the actual resizing of the image and show it
image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)

image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


# mask defaulting to black for 3-channel and transparent for 4-channel
# (of course replace corners with yours)
mask = np.zeros(image.shape, dtype=np.uint8)
roi_corners = np.array([[(914,1199), (161,1173), (185,462), (939,488)]], dtype=np.int32)

# fill the ROI so it doesn't get wiped out when the mask is applied
cv2.fillPoly(mask, roi_corners, 255)

# apply the mask
masked_image = cv2.bitwise_and(image, mask)

# save the result
cv2.imwrite('image_masked.png', masked_image)