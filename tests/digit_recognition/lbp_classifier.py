#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  2 12:21:34 2017

@author: max
"""
#import cv2
#from skimage.feature import local_binary_pattern
#import numpy as np
#
#image = cv2.imread('./../../output_images/29.png', 0)
#
## settings for LBP
#radius = 1
#n_points = 8 * radius
#
#lbp = local_binary_pattern(image, n_points, radius)
#lbp = np.reshape(lbp, np.size(lbp)).tolist()
#
#tryt = np.zeros((1,0))



import numpy as np
from sklearn.externals import joblib
from sklearn import datasets
from sklearn.neighbors import KNeighborsClassifier
from skimage.feature import local_binary_pattern

# settings for LBP
radius = 1
n_points = 8 * radius

###############
# load the MNIST digits dataset
dataset = datasets.fetch_mldata("MNIST Original")

# load necessary data
features = np.array(dataset.data, 'int16')
labels = np.array(dataset.target, 'int')

# Next, we calculate the HOG features for each image in the database and save them in another numpy array named hog_feature.
list_lbp_fd = []
for feature in features:
    reshaped = feature.reshape((28, 28))
    lbp = local_binary_pattern(reshaped, n_points, radius)
    lbp = np.reshape(lbp, (np.size(lbp))).tolist()
    list_lbp_fd.append(lbp)
lbp_features = np.array(list_lbp_fd, 'float32')

# initialize and train classifier
knn = KNeighborsClassifier(n_neighbors=3)
knn.fit(lbp_features, labels)

# save trained classifier
joblib.dump(knn, "digits_classificator_knn_lbp_32.pkl", compress=3)