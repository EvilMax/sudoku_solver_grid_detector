#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  2 13:38:44 2017

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.externals import joblib
from skimage.feature import hog
 

# lets load classifier
knn = joblib.load('./digits_classificator.pkl')


img = cv2.imread('./../../output_images/79.png',0)
size = np.size(img)
skel = np.zeros(img.shape,np.uint8)

backup = img.copy()

img = cv2.GaussianBlur(img, (5, 5), 10)
 
ret, img = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
img = 255-img

element = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))
done = False
 
while( not done):
    eroded = cv2.erode(img,element)
    temp = cv2.dilate(eroded,element)
    temp = cv2.subtract(img,temp)
    skel = cv2.bitwise_or(skel,temp)
    img = eroded.copy()
 
    zeros = size - cv2.countNonZero(img)
    if zeros==size:
        done = True
 
plt.figure(1)
plt.imshow(skel, cmap='gray')


skel = cv2.resize(skel, (28, 28), interpolation=cv2.INTER_AREA)

#skel = cv2.GaussianBlur(skel, (3, 3), 0)
#skel = cv2.dilate(skel,(3, 3), iterations=1)
plt.figure(2)
plt.imshow(skel, cmap='gray')

# calculate HOG features
roi_hog_fd = hog(skel, orientations=9, pixels_per_cell=(14, 14), cells_per_block=(1, 1), visualise = False)
nbr = knn.predict(np.array([roi_hog_fd], 'float32'))


cv2.putText(backup, str(int(nbr[0])), (10, 25),cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 1)

# final result    
plt.figure(3)
plt.imshow(backup, cmap='gray')