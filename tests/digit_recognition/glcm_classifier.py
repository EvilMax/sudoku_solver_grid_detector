#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  2 11:59:56 2017

@author: max
"""

import numpy as np
from skimage.io import imread
from skimage import img_as_ubyte
from skimage.feature import greycomatrix, greycoprops

 

image = imread('./../../output_images/29.png', as_grey=True)

# to convert to 8 levels
image = img_as_ubyte(image) / 32

g = greycomatrix(image, [1], [0], levels=8, symmetric=False, normed=True)


contrast= greycoprops(g, 'contrast')[0][0]
energy= greycoprops(g, 'energy')[0][0]
homogeneity= greycoprops(g, 'homogeneity')[0][0]
correlation=greycoprops(g, 'correlation')[0][0]
dissimilarity=greycoprops(g, 'dissimilarity')[0][0]
ASM=greycoprops(g, 'ASM')[0][0]


##g = greycomatrix(image, [0, 1], [0, np.pi/2], levels=256)
#
# 
#
#contrast = greycoprops(g, 'contrast')
#contrast = np.reshape(contrast, (1, np.size(contrast)))
##print('contrast is: ',  contrast)
#
# 
#
#energy = greycoprops(g, 'energy')
#energy = np.reshape(energy, (1, np.size(energy)))
##print('energy is: ',  energy)
#
# 
#
#homogeneity = greycoprops(g, 'homogeneity')
#homogeneity = np.reshape(homogeneity, (1, np.size(homogeneity)))
##print('homogeneity is: ',  homogeneity)
#
# 
#
#correlation = greycoprops(g, 'correlation')
#correlation = np.reshape(correlation, (1, np.size(correlation)))
##print('correlation is: ',  correlation)
#
# 
#
#dissimilarity = greycoprops(g, 'dissimilarity')
#dissimilarity = np.reshape(dissimilarity, (1, np.size(dissimilarity)))
##print('dissimilarity is: ',  dissimilarity)
#
# 
#
#ASM = greycoprops(g, 'ASM')
#ASM = np.reshape(ASM, (1, np.size(ASM)))
##print('ASM is: ',  ASM)