#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 30 11:42:09 2016

@author: max
"""

import numpy as np
from sklearn.externals import joblib
from sklearn import datasets
from sklearn.svm import LinearSVC
from skimage.feature import hog

###############
# load the MNIST digits dataset
dataset = datasets.fetch_mldata("MNIST Original")

# load necessary data
features = np.array(dataset.data, 'int16')
labels = np.array(dataset.target, 'int')

# Next, we calculate the HOG features for each image in the database and save them in another numpy array named hog_feature.
list_hog_fd = []
for feature in features:
    reshaped = feature.reshape((28, 28))
    fd = hog(reshaped, orientations=9, pixels_per_cell=(14, 14), cells_per_block=(1, 1), visualise=False)
    list_hog_fd.append(fd)
hog_features = np.array(list_hog_fd, 'float64')

# initialize and train classifier
clf = LinearSVC()
clf.fit(hog_features, labels)

# save trained classifier
joblib.dump(knn, "digits_classificator_svc_64.pkl", compress=3)