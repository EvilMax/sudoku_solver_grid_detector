#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 30 12:07:33 2016

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.externals import joblib
from skimage.feature import hog
from auto_canny import *


# lets load classifier
knn = joblib.load('./digits_classificator.pkl')

# read image
#11->1 works
img = cv2.imread('./../../output_images/6.png', 0)

gray = cv2.GaussianBlur(img, (5, 5), 0)

# threshold the image
_, gray = cv2.threshold(gray,127,255,cv2.THRESH_BINARY)
gray = 255-gray

gray = cv2.erode(gray, (5, 5), iterations=5)


#backup = None
#num_labels=1
#iterations = 0
#while(num_labels==1 and iterations<=10):
#    iterations += 1
#    
#    print("erosion")
#
#    backup = gray.copy()
#    
#    
#    gray = cv2.erode(gray, (5, 5), iterations=1)
#    
#    #erode until its needed
#    output = cv2.connectedComponentsWithStats(gray, 4, cv2.CV_32S)
#    # The first cell is the number of labels
#    num_labels = (output[0]-1)
#
#    # use last good result
#gray = backup


plt.figure(0)
plt.imshow(gray, cmap='gray')

##########OK LINE##################

roi = cv2.resize(gray, (28, 28), interpolation=cv2.INTER_LINEAR)
roi = cv2.GaussianBlur(roi, (5, 5), 0)
#roi = cv2.dilate(roi, (3, 3), iterations=1)



plt.figure(1)
plt.imshow(roi, cmap='gray')

# calculate HOG features
roi_hog_fd = hog(roi, orientations=9, pixels_per_cell=(14, 14), cells_per_block=(1, 1), visualise = False)
nbr = knn.predict(np.array([roi_hog_fd], 'float32'))


cv2.putText(img, str(int(nbr[0])), (10, 25),cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 1)

# final result    
plt.figure(2)
plt.imshow(img, cmap='gray')