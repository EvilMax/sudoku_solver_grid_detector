#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 30 12:07:33 2016

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.externals import joblib
from skimage.feature import hog
from auto_canny import *


# lets load classifier
knn = joblib.load('./digits_classificator.pkl')
#clf = joblib.load('./digits_classificator_svc.pkl')
#clf2 = joblib.load('./digits_classificator_svc_64.pkl')

# read image
#11->1 works
#img = cv2.imread('./num_8.jpg')
#img = cv2.imread('./num_9.jpg')
#img = cv2.imread('./num_7.jpg')
img = cv2.imread('./num_1.jpg')
#img = cv2.imread('./num_2.jpg')

# convert to grayscale and apply gaussian filtering
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#gray = cv2.GaussianBlur(gray, (5, 5), 10)

# threshold the image
gray = auto_canny(gray, 0.01)
#gray = cv2.erode(gray, (3, 3), iterations=1)
#gray = cv2.dilate(gray, (3, 3), iterations=5)
#gray = cv2.medianBlur(gray, 3)

#ret, gray = cv2.threshold(gray, 90, 255, cv2.THRESH_BINARY_INV)
#gray = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
#            cv2.THRESH_BINARY,11,2)
#gray = 255-gray
#gray = cv2.medianBlur(gray, 5)

#gray = cv2.erode(gray, (3, 3), iterations=10)
#gray = cv2.dilate(gray, (3, 3), iterations=10)


roi = cv2.resize(gray.copy(), (28, 28), interpolation=cv2.INTER_AREA)

#use not use dilation will work
#roi = cv2.dilate(roi, (3, 3), iterations=1)
#roi = cv2.erode(roi, (3, 3))

plt.figure(1)
plt.imshow(roi, cmap='gray')

# calculate HOG features
roi_hog_fd = hog(roi, orientations=9, pixels_per_cell=(14, 14), cells_per_block=(1, 1), visualise = False)
nbr = knn.predict(np.array([roi_hog_fd], 'float32'))
#nbr = clf.predict(np.array([roi_hog_fd], 'float32'))
#nbr = clf2.predict(np.array([roi_hog_fd], 'float64'))

cv2.putText(img, str(int(nbr[0])), (10, 25),cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 1)

# final result    
plt.figure(2)
plt.imshow(img)

#plt.figure(3)
#plt.imshow(gray, cmap='gray')