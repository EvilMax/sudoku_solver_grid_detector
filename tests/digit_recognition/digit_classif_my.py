#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 29 16:32:39 2016

@author: max
"""


import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_digits
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import train_test_split
from skimage.feature import hog
from auto_canny import *

## load the MNIST digits dataset
#digits = load_digits()
#
## take the MNIST data and construct the training and testing split, using 75% of the
## data for training and 25% for testing
#(trainData, testData, trainLabels, testLabels) = train_test_split(np.array(digits.data), digits.target, test_size=0.001)
#
#knn = KNeighborsClassifier(n_neighbors=3)
#knn.fit(trainData, trainLabels)
#
## predict
#predicted = knn.predict(testData)



###############
# load the MNIST digits dataset
digits = load_digits()


features = np.array(digits.images, 'int16')
labels = np.array(digits.target, 'int')

list_hog_fd = []
for feature in features:
    feature = cv2.resize(feature, (50, 50), interpolation=cv2.INTER_AREA)
    fd = hog(feature, orientations=9, pixels_per_cell=(50, 50), cells_per_block=(1, 1), visualise=False)
    #fd.reshape((1, len(fd)))
    #print(str(fd[1]))
    list_hog_fd.append( fd )
hog_features = np.array(list_hog_fd, 'float32')

plt.figure(1)
plt.imshow(cv2.dilate(cv2.resize(features[0], (50, 50), interpolation=cv2.INTER_AREA), (3,3), iterations=1), cmap='gray')


my_num = cv2.imread('./num_7.jpg', 0)
#my_num = cv2.imread('./num_9.jpg', 0)
my_num = cv2.resize(my_num, (50, 50), interpolation=cv2.INTER_AREA)
#my_num = 255-my_num
my_num = auto_canny(my_num, 0.99)
#my_num = cv2.dilate(my_num, (3, 3), iterations=1)

plt.figure(2)
plt.imshow(my_num, cmap='gray')

#find hog features
my_hog_f = hog(my_num, orientations=9, pixels_per_cell=(50, 50), cells_per_block=(1, 1), visualise=False)
my_hog_f = np.array([my_hog_f], 'float32')


knn = KNeighborsClassifier(n_neighbors=3)
knn.fit(hog_features, labels)


# detect num
loaded_num = knn.predict(my_hog_f)
print(str(loaded_num))