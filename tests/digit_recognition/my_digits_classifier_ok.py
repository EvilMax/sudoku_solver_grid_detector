#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 30 12:07:33 2016

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.externals import joblib
from skimage.feature import hog
from auto_canny import *


# lets load classifier
#knn = joblib.load('./digits_classificator.pkl')
#clf = joblib.load('./digits_classificator_svc.pkl')
clf2 = joblib.load('./digits_classificator_svc_64.pkl')

# read image
#img = cv2.imread('./digit-reco-1-in.jpg')
#img = cv2.imread('./numbers2.jpg')
img = cv2.imread('./num_9.jpg')

# convert to grayscale and apply gaussian filtering
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
gray = cv2.GaussianBlur(gray, (5, 5), 0)

# threshold the image
#gray = auto_canny(gray)
ret, gray = cv2.threshold(gray, 90, 255, cv2.THRESH_BINARY_INV)
#gray = cv2.erode(gray, (3, 3), iterations=10)

# find contours
_, contours, _ = cv2.findContours(gray.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

#build rectangle for each contour
rects = [cv2.boundingRect(ctr) for ctr in contours]

counter = 100

# For each rectangular region, calculate HOG features and predict
for rect in rects:
    
    counter += 1
    x = rect[0]
    y = rect[1]
    w = rect[2]
    h = rect[3]
    more = 0
#    length=w
#    if(h > length):
#        length = h
    
    # draw rectangle
    cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 3)
    
    #fetch region content
#    leng = int(w * 1.6)
#    pt1 = int(y + h // 2 - leng // 2)
#    pt2 = int(x + w // 2 - leng // 2)
#    
    #roi = gray[pt1:pt1+leng, pt2:pt2+leng]

    #roi = gray[y-more:y+h+more, x-more:x+h+more]
    roi = gray[y-more:y+h+more, x-more:x+w+more]
    
    # resize founded roi
    roi = cv2.resize(roi, (28, 28), interpolation=cv2.INTER_AREA)
    roi = cv2.dilate(roi, (3, 3))
    #roi = cv2.erode(roi, (3, 3))
    
    plt.figure(counter)
    plt.imshow(roi, cmap='gray')
    
    # calculate HOG features
    roi_hog_fd = hog(roi, orientations=9, pixels_per_cell=(14, 14), cells_per_block=(1, 1), visualise = False)
    #nbr = knn.predict(np.array([roi_hog_fd], 'float32'))
    #nbr = clf.predict(np.array([roi_hog_fd], 'float32'))
    nbr = clf2.predict(np.array([roi_hog_fd], 'float64'))
    
    cv2.putText(img, str(int(nbr[0])), (rect[0], rect[1]+round(rect[3]/1)),cv2.FONT_HERSHEY_DUPLEX, 2, (0, 255, 255), 3)

# final result    
plt.figure("Final_Result")
plt.imshow(img)

plt.figure(2)
plt.imshow(gray, cmap='gray')