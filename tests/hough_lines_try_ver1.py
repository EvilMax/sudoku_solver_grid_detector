#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 26 12:12:38 2016

@author: max
"""

import cv2
import math
import numpy as np
import matplotlib.pyplot as plt
from auto_canny import *



#img =  cv2.imread('./first.jpg')
img =  cv2.imread('./../images/4.jpg')


#resize image to default size by keeping ratio
width = 1280
r = float(width) / img.shape[1]
height = int(img.shape[0] * r)
dim = (width, height)
 
# perform the actual resizing of the image and show it
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

Y = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)

width, height = Y.shape

# adaptative histogramm gamma correction
# create a CLAHE object (Arguments are optional).
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
Y = clahe.apply(Y)

Y = cv2.GaussianBlur(Y, (5, 5), 0)

thresh = auto_canny(Y)

#thresh = cv2.medianBlur(thresh,3)

kernel = np.ones((5, 5), np.uint8)
thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

#plt.figure(4)
#plt.imshow(thresh, cmap='gray')



fixed_grid = thresh.copy()

minLineLength = 50
maxLineGap = 1
lines = cv2.HoughLinesP(thresh,1,np.pi/180,10,minLineLength,maxLineGap)
for x in range(0, len(lines)):
    for x1,y1,x2,y2 in lines[x]:
        cv2.line(fixed_grid,(x1,y1),(x2,y2),(255,255,255),4)


#plt.figure(5)
#plt.imshow(fixed_grid, cmap='gray')

_, contours, _ = cv2.findContours(fixed_grid.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

#print(str(hierarchy))

result = cv2.cvtColor(fixed_grid.copy(), cv2.COLOR_GRAY2RGB)

biggest = None
max_area = 0

#right_cnt=[]

for cnt in contours:
        area = cv2.contourArea(cnt)
        if area > width:
                epsilon = 0.05 * cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, epsilon, True)
                
           
                #draw also rectangle to save time:
                if area > width and len(approx)>=4 and len(approx)<=8:
                    
                    x, y, w, h = cv2.boundingRect(cnt)
                    
                    #take just only things similar to squares
                    ar = w / float(h)
                    
                    if(ar >= 0.9 and ar <= 1.2):
                        
                        cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)
                        
                        #right_cnt.append(cv2.boundingRect(cnt))
                        #right_cnt.append(approx)
                        
                        # update maximum rectangle found
                        # if its a quad +-
                        if area > max_area:
                            
                            biggest = approx
                            max_area = area
                        
    
#plt.figure(1)
#plt.imshow(Y, cmap='gray')
#
#
#plt.figure(2)
#plt.imshow(result)


x, y, w, h = cv2.boundingRect(biggest)
# to get image
backup=fixed_grid
fixed_grid = Y[y:y+h, x:x+w]
fixed_grid_mask = backup[y:y+h, x:x+w]
# to get borders
#fixed_grid = fixed_grid[y:y+h, x:x+w]


#resize image to default size by keeping ratio
width = 1280
r = float(width) / fixed_grid.shape[1]
dim = (width, int(fixed_grid.shape[0] * r))

# perform the actual resizing of the image and show it
fixed_grid = cv2.resize(fixed_grid, dim, interpolation = cv2.INTER_AREA)
fixed_grid_mask = cv2.resize(fixed_grid_mask, dim, interpolation = cv2.INTER_AREA)


#plt.figure("Grid_found_step_2")
#plt.imshow(fixed_grid, cmap='gray')
#
#plt.figure("Grid_mask_found_step_2")
#plt.imshow(fixed_grid_mask, cmap='gray')


############################################
# WORK ON FOUND SUDOKU GRID
############################################


# ( center (x,y), (width, height), angle of rotation )
minRect = cv2.minAreaRect(biggest)
box = cv2.boxPoints(minRect)

box = np.int0(box)
cv2.drawContours(result,[box],0,(255,0,0),3)

#print("POINST: "+str(box[1][0])+"   "+str(box[1][1]))

angle = minRect[2]
edge1 = np.array( (box[1][0], box[1][1]) ) - np.array( (box[0][0], box[0][1]) )
edge2 = np.array( (box[2][0], box[2][1]) ) - np.array( (box[1][0], box[1][1]) )

#select biggest edge to compute angle
used_edge = edge1
if(cv2.norm(edge2) > cv2.norm(edge1)):
    used_edge = edge2
    
#horizontal line to calculate rotation angle
reference = np.array( (1, 0) )

#print(str(edge1))

#compute correctly angle of rotation
angle = 180.0/math.pi * math.acos( 
                                  (reference[0] * used_edge[0] + reference[1] * used_edge[1]) /
                                  ( cv2.norm(reference) * cv2.norm(used_edge) ) 
                                 )

print(str(angle))



#if( not( (abs(angle)>=85 and abs(angle)<=95) or (abs(angle)>=175 and abs(angle)<=185) ) ):
if( not( (abs(angle)>=175 and abs(angle)<=185) ) ):

    #final_rotation = -((90-angle)/4)
    final_rotation = -round(angle)
    
    #lets rotate
    M = cv2.getRotationMatrix2D((width/2,height/2), final_rotation, 1)
    
    fixed_grid = cv2.warpAffine(fixed_grid,M,(width, height))
    fixed_grid_mask = cv2.warpAffine(fixed_grid_mask,M,(width, height))

#fixed_grid = crop_around_center(fixed_grid, fixed_grid.shape[0], fixed_grid.shape[1])

#plt.figure("Grid_rotated_step_2")
#plt.imshow(fixed_grid, cmap='gray')
#

fixed_grid_mask = cv2.GaussianBlur(fixed_grid_mask, (9,9), 0)

#minLineLength = 50
#maxLineGap = 1
#lines = cv2.HoughLinesP(fixed_grid_mask,1,np.pi/180,10,minLineLength,maxLineGap)
#for x in range(0, len(lines)):
#    for x1,y1,x2,y2 in lines[x]:
#        cv2.line(fixed_grid_mask,(x1,y1),(x2,y2),(255,255,255),4)

#fix lines if needed
lines= cv2.HoughLines(fixed_grid_mask.copy(), 1, math.pi/180.0, 1100, np.array([]), 0, 0)

a,b,c = lines.shape
for i in range(a):
    rho = lines[i][0][0]
    theta = lines[i][0][1]
    a = math.cos(theta)
    b = math.sin(theta)
    x0, y0 = a*rho, b*rho
    pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
    pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
    cv2.line(fixed_grid_mask, pt1, pt2, (255, 255, 255), 5, cv2.LINE_AA)

plt.figure("Grid_mask_rotated_step_2")
plt.imshow(fixed_grid_mask, cmap='gray')

########################################
## CROP NEWELY GRID
#######################################


_, contours, _ = cv2.findContours(fixed_grid_mask.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
##contours = sorted(contours, key=cv2.contourArea, reverse=True)
#right_cnt = []
#for cnt in contours:
#        area = cv2.contourArea(cnt)
#        if area > width:
#                epsilon = 0.05 * cv2.arcLength(cnt, True)
#                approx = cv2.approxPolyDP(cnt, epsilon, True)
#                
#           
#                #draw also rectangle to save time:
#                if area > width and area < (width*height)/2 and len(approx)>=4 and len(approx)<=8:
#                    x, y, w, h = cv2.boundingRect(cnt)
#                    
#                    #take just only things similar to squares
#                    ar = w / float(h)
#                    
#                    if(ar >= 0.9 and ar <= 1.2):
#                        right_cnt.append(approx)
#
##(contours, boundingBoxes) = sort_contours(contours)
##tmp = np.array((cv2.boundingRect(right_cnt)[0], cv2.boundingRect(right_cnt)[1]))
#contours = sorted(contours, key=lambda s: ((cv2.boundingRect(s)[1], cv2.boundingRect(s)[0])), reverse=False)


#####àdeletable##############
#m=9
#n=9
#
#sudoku_cells = []
#for i in range(0,m):
#    sudoku_cells.append([])
#    for j in range(0,n):
#        sudoku_cells[i].append(0)
#
#
#
#for i in range(m):
#    for j in range(n):
#        
#        if(i==0 and j==0):
#            sudoku_cells[i][j] = contours[0]
#        
#        #neighbour = next( (x for x in contours if cv2.boundingRect(x)[0] > cv2.boundingRect(sudoku_cells[i][j-1])[0]) )
#        
#        sudoku_cells[i][j] = neighbour
#        
#print(str(sudoku_cells))        
########################

#lets try to sort
#minimum = contours[0]
#for i in range(1, len(contours)):
#    x,y,w,h = cv2.boundingRect(contours[i])
#    print(str)
    #sudoku_cells[][]

#print(str(hierarchy))

#for working with labels
grid_labels = np.zeros( fixed_grid_mask.shape, dtype='uint8' )

result3 = cv2.cvtColor(fixed_grid.copy(), cv2.COLOR_GRAY2RGB)

biggest = None
max_area = 0
counter = 0

w1=0
h1 = 0

for cnt in contours:
        area = cv2.contourArea(cnt)
        if area > width:
                epsilon = 0.05 * cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, epsilon, True)
                
           
                #draw also rectangle to save time:
                if area > width and area < (width*height)/50 and len(approx)>=4 and len(approx)<=8:
                #if area > width and len(approx)>=4 and len(approx)<=8:
                  
                    
                    x, y, w, h = cv2.boundingRect(cnt)
                    
                    #take just only things similar to squares
                    ar = w / float(h)
                    
                    if(ar >= 0.85 and ar <= 1.2):
                        counter = counter + 1
                
                        cv2.rectangle(result3, (x, y), (x+w, y+h), (0, 255, 0), 2)
                        
                        w1=w
                        h1=h
                                                
                        # add also some text
                        # Write some Text
                        font = cv2.FONT_HERSHEY_SIMPLEX
                        cv2.putText(result3, str(counter),(x+10,y+20), font, 2,(255,255,255),2)
                        
                        #fill correctly grid labels with white spaces
                        grid_labels[y:y+h, x:x+w] = 255
                        
                        # update maximum rectangle found
                        # if its a quad +-
                        if area > max_area:
                            
                            biggest = approx
                            max_area = area
                        
    
plt.figure("Mask_cropped_newewly")
plt.imshow(result3, cmap='gray')

#executwe operation of label naming

# You need to choose 4 or 8 for connectivity type
connectivity = 4  
# Perform the operation
output = cv2.connectedComponentsWithStats(grid_labels.copy(), connectivity, cv2.CV_32S)
# Get the results
# The first cell is the number of labels
num_labels = output[0]
# The second cell is the label matrix
labels = output[1]
# The third cell is the stat matrix
stats = output[2]
# The fourth cell is the centroid matrix
centroids = output[3]


w, h = labels.shape

#if i shart from zero there will be erroneous override, solutioons: do like here, or change all numbers beore and then start counter from 0
#counter = num_labels
#for i in range(round(w/16), w, round(w/9)):
#    for j in range(round(h/16), h, round(h/9)):
#        
#        #will ruin result but useful for debug
#        #cv2.circle(labels,(i,j), 10, (0,0,0), -1)
#        #print("Point: "+str(i)+" "+str(j)+" | "+str(labels[i][j]))
#        
#        if(labels[i][j] != 0):
#            
#            # Write some Text
#            #font = cv2.FONT_HERSHEY_SIMPLEX
#            #cv2.putText(grid_labels, str(counter),(i,j), font, 1,(255,255,255),2)
#            
#            #print("match"+str(counter))
#            print(str(labels[i][j])+" assigned -> "+ str(counter))
#            
#            counter = counter+1
#            #labels[labels==labels[i][j]] = counter
#            np.place(labels, labels==labels[i,j], counter)
#            #counter = counter+1

#better algorithm
step=20

need_change = True
counter = num_labels+1
first=True
for i in range(0, w, step):
    old_counter=counter
    need_change=True
    first=True
    for j in range(0, h, step):
        
        #debug, but breaks result
        #cv2.circle(labels,(i,j), 10, (0,0,0), -1)
        
        if(i>120 and j>j*step and first==True):
            cv2.circle(labels,(i,j), 10, (255,255,255), -1)
            need_change=False
            first=False
        
        if(labels[i][j] != 0 and need_change==True and not(labels[i][j]>=(num_labels+1) and labels[i][j]<=counter )):
            labels[labels==labels[i][j]] = counter
            counter += 1
            need_change = True
            first=False
            #cv2.circle(labels,(i,j), 10, (0,0,0), -1)
        
#        if(labels[i][j] == 0 and need_change==False):
#            need_change = True
        
plt.figure("Labeling")
plt.imshow(labels, cmap='gray')        

#tryy=[]
#counter = 0
#for n in range(1, num_labels):
#    counter = counter+3
#    labels[labels==n] = counter
    #print(str(labels[labels==n]))
    #np.where(labels==n) = 255