#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 23 13:08:04 2016

@author: max
"""

import cv2
import matplotlib.pyplot as plt
import numpy as np

####################################################
#img = cv2.imread("./../images/first.jpg")
img = cv2.imread("./../images/20161128_113241027.jpg")
height, width = img[:, :, 0].shape

ratio = 0.1
new_height = round(ratio*height)
new_width = round(ratio*width)

# now resize for better performance
resized_img = cv2.resize(img, (new_width, new_height),
                     interpolation=cv2.INTER_AREA)

#get just 1 channel
ycrcb1 = cv2.cvtColor(resized_img, cv2.COLOR_BGR2YCR_CB)
Y_img = ycrcb1[:, :, 0]
####################################################

####################################################
template = cv2.imread("./sudoku_template_2.jpg")
height, width = template[:, :, 0].shape

#get just 1 channel
ycrcb2 = cv2.cvtColor(template, cv2.COLOR_BGR2YCR_CB)
Y_template = ycrcb2[:, :, 0]
####################################################

plt.figure(1)
plt.subplot(211), plt.imshow(Y_img, cmap='gray'), plt.title('Original')
plt.axis("off")
plt.subplot(212), plt.imshow(Y_template, cmap='gray'), plt.title('Template')
plt.axis("off")

####################################################
####################################################
####################################################

match_result = None

# Initiate SIFT detector
orb = cv2.ORB_create()

# find the keypoints and descriptors with SIFT
kp1, des1 = orb.detectAndCompute(Y_img, None)
kp2, des2 = orb.detectAndCompute(Y_template, None)

# create BFMatcher object
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

# Match descriptors.
matches = bf.match(des1,des2)

# Sort them in the order of their distance.
matches = sorted(matches, key = lambda x:x.distance)

# Draw first 100 matches.
img3 = cv2.drawMatches(Y_img,kp1,Y_template,kp2,matches[:100],match_result,flags=2)

plt.figure(2)
plt.imshow(img3)
plt.axis('off')
