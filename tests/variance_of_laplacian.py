#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 23 17:29:07 2016

@author: max
"""

# import the necessary packages
import cv2
 
def variance_of_laplacian(image):
	# compute the Laplacian of the image and then return the focus
	# measure, which is simply the variance of the Laplacian
	return cv2.Laplacian(image, cv2.CV_64F).var()


 
 #img = cv2.imread("./../images/first.jpg")
img = cv2.imread("./../images/20161128_113502.jpg")
height, width = img[:, :, 0].shape

ratio = 0.1
new_height = round(ratio*height)
new_width = round(ratio*width)

# now resize for better performance
resized_img = cv2.resize(img, (new_width, new_height),
                     interpolation=cv2.INTER_AREA)

#get just 1 channel
ycrcb1 = cv2.cvtColor(resized_img, cv2.COLOR_BGR2YCR_CB)
Y = ycrcb1[:, :, 0]

threshold = 750

fm = variance_of_laplacian(Y)

print(str(fm))

if(fm < threshold):
    print("Blurred")
    
    
    
else:
    print("The image isnt blurred!")