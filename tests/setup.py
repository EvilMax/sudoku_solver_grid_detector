import sys
from cx_Freeze import setup, Executable

includes = []
excludes = []
packages = ['numpy', 'matplotlib', 'tkinter', 'cv2']
path = []
base = None
if sys.platform == 'win32':
    base = 'Win32GUI'
options = {
    'build_exe': {
        "includes": includes,
        "excludes": excludes,
        "packages": packages,
        "path": path
        #'excludes': ['Tkinter']  # Sometimes a little finetuning is needed
    }
}
executables = [Executable('nuitka.py', base=base)]
setup(name='nuitka',
      version='0.1',
      description='Sample PyQT5-matplotlib script',
      executables=executables,
      options=options
      )