#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  5 18:19:45 2017

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt


labels = np.load("./../labels.npy")

height, width = labels.shape

##better algorithm
#step=20
#
#num_labels=82
#
#counter = 83 #83
#first=True
#for i in range(0, height, step):
#    first=True
#    
#    for j in range(0, width, step*2):
#        
#        if(j>120 and first==True):
#            break
#        else:
#            #print(str(first))
#            if(labels[i][j] != 0 and not(labels[i][j]>=(num_labels+1) and labels[i][j]<=counter )):
#                labels[labels==labels[i][j]] = counter
#                counter += 1
#                first=False
#            #cv2.circle(labels,(i,j), 10, 75, -1)





        
#print("Will be rotated by: "+str(4)+" degrees to left!")
#
##lets rotate
#M = cv2.getRotationMatrix2D((width/2,height/2), -4, 1)
#
#labels = cv2.warpAffine(labels, M, (width, height))



tmp_array = []
#matrix = []

#better algorithm
step=10

num_labels=82
counter = 83
first=True

already_seen = None

for i in range(0, height, step):
    if(len(tmp_array) > 0 and len(tmp_array) < 9):
        first=False
        tmp_array = []
    else:
        #ignore=0
        #matrix.append(tmp_array)
        for i in range(0, len(tmp_array)):
            a = tmp_array[i][0]
            b = tmp_array[i][1]
            labels[labels==labels[a][b]] = counter
            counter += 1
        first=True
        tmp_array=[]
        
    for j in range(0, width, step*2):
        

        #this check is useful if cell wasnt detected before and its in the same row of detected cells but its smaller
#        if(labels[i][j] != 0 and ( labels[i][j]>=(num_labels+1) and labels[i][j]<=counter ) ):
#            tmp_array.append(0)
        
        #1280/9=142...
        if(j>140 and first==True):
            # if we havent found any cell before pixel 120 ignore whole line
            break
                
        else:
            #print(str(first))
            if((labels[i][j] != already_seen or already_seen is None) and labels[i][j] != 0 and not( labels[i][j]>=(num_labels+1) and labels[i][j]<=counter )):
#                labels[labels==labels[i][j]] = counter
#                counter += 1
                already_seen = labels[i][j]
                first=False
                tmp_array.append([i, j])
                #cv2.circle(labels,(i,j), 5, 165, -1)
                
        



   
plt.figure(1)
plt.imshow(labels, cmap='gray')