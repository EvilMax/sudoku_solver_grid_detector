#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 26 12:12:38 2016

@author: max
"""

import cv2
import math
import numpy as np
import matplotlib.pyplot as plt
from auto_canny import *


#print(filename)
#img =  cv2.imread('./first.jpg')
img =  cv2.imread('./../images/20161128_115306.jpg')


#resize image to default size by keeping ratio
width = 1280
r = float(width) / img.shape[1]
height = int(img.shape[0] * r)
dim = (width, height)
 
# perform the actual resizing of the image and show it
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

Y = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)

width, height = Y.shape

# adaptative histogramm gamma correction
# create a CLAHE object (Arguments are optional).
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
Y = clahe.apply(Y)

Y = cv2.GaussianBlur(Y, (5, 5), 0)

thresh = auto_canny(Y)

#thresh = cv2.medianBlur(thresh,3)

kernel = np.ones((5, 5), np.uint8)
thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

plt.figure(4)
plt.imshow(thresh, cmap='gray')

#
#lines= cv2.HoughLines(thresh.copy(), 1, math.pi/180.0, 200, np.array([]), 0, 0)
#
#a,b,c = lines.shape
#for i in range(a):
#    rho = lines[i][0][0]
#    theta = lines[i][0][1]
#    a = math.cos(theta)
#    b = math.sin(theta)
#    x0, y0 = a*rho, b*rho
#    pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
#    pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
#    cv2.line(Y, pt1, pt2, (255, 255, 255), 10, cv2.LINE_AA)


fixed_grid = thresh.copy()

minLineLength = 50
maxLineGap = 1
lines = cv2.HoughLinesP(thresh,1,np.pi/180,10,minLineLength,maxLineGap)
for x in range(0, len(lines)):
    for x1,y1,x2,y2 in lines[x]:
        cv2.line(fixed_grid,(x1,y1),(x2,y2),(255,255,255),4)


plt.figure(5)
plt.imshow(fixed_grid, cmap='gray')

_, contours, _ = cv2.findContours(fixed_grid.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

#print(str(hierarchy))

result = cv2.cvtColor(fixed_grid.copy(), cv2.COLOR_GRAY2RGB)

biggest = None
max_area = 0
for cnt in contours:
        area = cv2.contourArea(cnt)
        if area > width:
                epsilon = 0.05 * cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, epsilon, True)
                
           
                #draw also rectangle to save time:
                if area > width and len(approx)>=4 and len(approx)<=8:
                    
                    x, y, w, h = cv2.boundingRect(cnt)
                    
                    #take just only things similar to squares
                    ar = w / float(h)
                    
                    if(ar >= 0.9 and ar <= 1.2):
                    
                        cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)
                
                        # update maximum rectangle found
                        # if its a quad +-
                        if area > max_area:
                            
                            biggest = approx
                            max_area = area
                        
    
plt.figure(1)
plt.imshow(Y, cmap='gray')


plt.figure(2)
plt.imshow(result)


x, y, w, h = cv2.boundingRect(biggest)
# to get image
fixed_grid = Y[y:y+h, x:x+w]
# to get borders
#fixed_grid = fixed_grid[y:y+h, x:x+w]


#resize image to default size by keeping ratio
width = 1280
r = float(width) / fixed_grid.shape[1]
dim = (width, int(fixed_grid.shape[0] * r))

# perform the actual resizing of the image and show it
fixed_grid = cv2.resize(fixed_grid, dim, interpolation = cv2.INTER_AREA)


plt.figure(55)
plt.imshow(fixed_grid, cmap='gray')


############################################
# WORK ON FOUND SUDOKU GRID
############################################

#gaussian_3 = cv2.GaussianBlur(fixed_grid, (9,9), 10.0)
#grid = cv2.addWeighted(fixed_grid, 1.5, gaussian_3, -0.5, 0, fixed_grid)

#grid = cv2.GaussianBlur(fixed_grid, (5, 5), 0)
grid = auto_canny(fixed_grid, sigma=0.01)

kernel2 = np.ones((3, 3), np.uint8)
grid = cv2.dilate(grid, kernel2, iterations=4)


minLineLength = 50
maxLineGap = 1
lines = cv2.HoughLinesP(grid,1,np.pi/180,10,minLineLength,maxLineGap)
for x in range(0, len(lines)):
    for x1,y1,x2,y2 in lines[x]:
        cv2.line(grid,(x1,y1),(x2,y2),(255,255,255),2)


plt.figure("Grid_step_2")
plt.imshow(grid, cmap='gray')


# find again contours, but now with min area rect
_, contours2, _ = cv2.findContours(grid.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

result2 = cv2.cvtColor(grid.copy(), cv2.COLOR_GRAY2RGB)

biggest = None
max_area = 0
for cnt in contours2:
        area = cv2.contourArea(cnt)
        if area > width:
                epsilon = 0.05 * cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, epsilon, True)
                
           
                #draw also rectangle to save time:
                if area > width and len(approx)>=4 and len(approx)<=8:
                    
                    # ( center (x,y), (width, height), angle of rotation )
                    minRect = cv2.minAreaRect(cnt)
                    box = cv2.boxPoints(minRect)
                    
                    #take just only things similar to squares
                    ar = w / float(h)
                    
                    if(ar >= 0.9 and ar <= 1.2):
                    
                        
                        box = np.int0(box)
                        cv2.drawContours(result2,[box],0,(0,255,0),2)
                        
                        # update maximum rectangle found
                        # if its a quad +-
                        if area > max_area:
                            
                            biggest = approx
                            max_area = area

# ( center (x,y), (width, height), angle of rotation )
minRect = cv2.minAreaRect(biggest)
box = cv2.boxPoints(minRect)

#print("POINST: "+str(box[1][0])+"   "+str(box[1][1]))

angle = minRect[2]
edge1 = np.array( (box[1][0], box[1][1]) ) - np.array( (box[0][0], box[0][1]) )
edge2 = np.array( (box[2][0], box[2][1]) ) - np.array( (box[1][0], box[1][1]) )

#select biggest edge to compute angle
used_edge = edge1
if(cv2.norm(edge2) > cv2.norm(edge1)):
    used_edge = edge2
    
#horizontal line to calculate rotation angle
reference = np.array( (1, 0) )

#print(str(edge1))

#compute correctly angle of rotation
angle = 180.0/math.pi * math.acos( 
                                  (reference[0] * used_edge[0] + reference[1] * used_edge[1]) /
                                  ( cv2.norm(reference) * cv2.norm(used_edge) ) 
                                 )

print(str(angle))



                            
plt.figure("Grid_identified_step_2")
plt.imshow(result2, cmap='gray')

#final_rotation = -((90-angle)/4)
final_rotation = -round(angle)

#lets rotate
M = cv2.getRotationMatrix2D((width/2,height/2), final_rotation, 1)
grid = cv2.warpAffine(grid,M,(width, height))

plt.figure("Grid_rotated_step_2")
plt.imshow(grid, cmap='gray')