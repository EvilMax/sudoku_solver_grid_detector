#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 25 10:10:55 2016

@author: max
"""
import cv2
import numpy as np
from matplotlib import pyplot as plt


class Grid_Fixer:

    def __init__(self, edges, percentage, debug, subparts=2):
        
        if(subparts%2 != 0):
            print("ERROR: subparts must be multiple of 2!")
            print("Examples: 2 4 8 16 32 64 128 256 ...")
            return
        
        self.kernel = np.ones((3, 3), np.uint8)
        self.adjusted_edges = self.rebuild_grid_detection_errors(edges, self.kernel, percentage, subparts, debug)
        
    
    def round_down(self, num, divisor):
        return (num - (num % divisor))

    
    def rebuild_grid_detection_errors(self, edges, kernel, percentage, subparts, debug=False):

       if(subparts >= 4):
           if(debug==True):
               return self.rebuild_sub_grids(edges, kernel, percentage, subparts, debug=True)
           else:
               return self.rebuild_sub_grids(edges, kernel, percentage, subparts, debug=False)
           
       else:
           
           height, width = edges.shape
           
           #dilate more if needed
           tot_wh_pixels = round((np.count_nonzero(edges == 255) )/(height*width), 2)
           while tot_wh_pixels <= percentage:
                
               edges = cv2.dilate(edges, kernel, iterations=1)
                
               tot_wh_pixels = round((np.count_nonzero(edges == 255) )/(height*width), 2)
               
               if(debug == True):
                   print("Number of white pixels / whole img: "+ str(tot_wh_pixels))
           
           if(debug == True):
               plt.figure("Remade")
               plt.imshow(edges, cmap='gray')
               
           return edges
    
      
    def rebuild_sub_grids(self, edges, kernel, percentage, subparts, debug=False):
        
        height, width = edges.shape
        
        #subparts = 16
        new_height = self.round_down(height, subparts)
        new_width = self.round_down(width, subparts)
        
        #resize to be able to split image in equal parts
        if(height != new_height or width != new_width):
            edges = cv2.resize(edges, (new_width, new_height), interpolation=cv2.INTER_AREA)
        
            
        result = []
        
        row_result = None
        col_result = None
        # finally split in sub quads of arbitrary dimensions
        sub_img = np.split(edges, subparts/2)
        for i in range(0, len(sub_img)):
            #print("aaaa")
            
            tmp = np.hsplit(sub_img[i], subparts/2)
            for j in range(0, len(tmp)):
                
                #do operations on sub matrixes here
                #result
                
                dilated = self.adjust_subimage(tmp[j], kernel, percentage, debug)
                
                result.append(dilated)
                
                #print("bbbb")
                #plt.figure(str(i)+str(j))
                #plt.imshow(tmp[j], cmap='gray')
        
        #print(str(len(result)))

        tmp_rows = []    
        tmp_cols = None
        for i in range(0, len(result), int(subparts/2)):
            
            sub_tmp = None
            cnt = i
            
            for j in range(0, int(subparts/2), 2):
                
                
                #print("ssssss")
                if(j != 0):
                    sub_tmp1 = sub_tmp
                    
                sub_tmp = np.concatenate((result[cnt], result[cnt+1]), axis=1 )
                
                if(j != 0):
                    sub_tmp = np.concatenate((sub_tmp1, sub_tmp), axis=1)
            
                cnt= cnt + 2
                
                
            #print(str(sub_tmp.shape))
            tmp_rows.append(sub_tmp)

        #print(str(len(tmp_rows)))
        for i in range(0, len(tmp_rows), 2):
            if(i != 0):
                sub_tmp2 = tmp_cols

            tmp_cols = np.concatenate((tmp_rows[i], tmp_rows[i+1]), axis=0)
            
            if(i != 0):
                tmp_cols = np.concatenate((sub_tmp2, tmp_cols), axis=0)
        
                
        return (tmp_cols)
        
        if(debug == True):
            plt.figure("REMERGED")
            plt.imshow(tmp_cols, cmap='gray')
            
        
    
    def adjust_subimage(self, subimg, kernel, percentage, debug):
        
        height, width = subimg.shape
        
        #dilate more white regions if needed
        tot_wh_pixels = round( (np.count_nonzero(subimg == 255)) / (height*width), 2)

        if(debug == True):
            print("Number of white pixels / sub_matrix: " + str(tot_wh_pixels))

        # 0.2 estimated on best image
        while tot_wh_pixels <= percentage:

            subimg = cv2.dilate(subimg, kernel, iterations=1)

            tot_wh_pixels = round((np.count_nonzero(subimg == 255) )/(height*width), 2)

            #print("Number of white pixels / whole img: " + str(tot_wh_pixels))
        
        if(tot_wh_pixels > percentage+0.2):
            while(tot_wh_pixels-percentage <= 0.1):
                subimg = cv2.erode(subimg, kernel, iterations=1)
                tot_wh_pixels = round((np.count_nonzero(subimg == 255) )/(height*width), 2)

            
        return (subimg)