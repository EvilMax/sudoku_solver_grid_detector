#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 14:56:45 2016

@author: max
"""
# just to clear workspace in Spyder3
#from IPython import get_ipython
#ipython = get_ipython()
#ipython.magic("%reset -f")
###################################


import cv2
import numpy as np
from matplotlib import pyplot as plt

from Loader import *
from Grid_Fixer import *
from Grid_Detector import *


# PREPROCESSING
# load image and do first standard steps
img = Loader("./images/6.jpg")

plt.figure("Y_channel")
plt.imshow(img.Y, cmap='gray')

print("Image size: " + str(img.size))

#lines = cv2.HoughLines(img.edges,1,np.pi/180,10)
#for rho,theta in lines[0]:
#    a = np.cos(theta)
#    b = np.sin(theta)
#    x0 = a*rho
#    y0 = b*rho
#    x1 = int(x0 + 1000*(-b))
#    y1 = int(y0 + 1000*(a))
#    x2 = int(x0 - 1000*(-b))
#    y2 = int(y0 - 1000*(a))
#
#    cv2.line(img.edges,(x1,y1),(x2,y2),(255,255,255),2)

grid = Grid_Fixer(img.edges, percentage=0.1, debug=False, subparts=2)

# can be used to detect mask or region
detected_grid = Grid_Detector(img.Y, grid.adjusted_edges, objective="region", debug=True)



plt.figure("Dilated_Edges")
plt.imshow(grid.adjusted_edges.astype(int), cmap='gray')