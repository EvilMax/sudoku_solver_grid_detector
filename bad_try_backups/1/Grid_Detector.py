#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 25 14:59:26 2016

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt


class Grid_Detector:
    
    def __init__(self, img, edges, objective="region", debug=False):
        
        self.grid_found = self.find_contours(img, edges, objective, debug)
        
        self.result = self.resize_founded_grid_to_default(self.grid_found, debug)
        
    def find_contours(self, img, edges, objective, debug):
        
        height, width = edges.shape
        
        _, contours, _ = cv2.findContours(edges.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
        result = cv2.cvtColor(edges.copy(), cv2.COLOR_GRAY2RGB)

        
        biggest = None
        max_area = 0
        for cnt in contours:
                area = cv2.contourArea(cnt)
                if area > width:
                        epsilon = 0.05 * cv2.arcLength(cnt, True)
                        approx = cv2.approxPolyDP(cnt, epsilon, True)
                        
                        
                        
                        #draw also rectangle to save time:
                        if area > width and len(approx)>=4 and len(approx)<=5:
                            x, y, w, h = cv2.boundingRect(cnt)
                            cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)
                        
                        
                        if area > max_area and len(approx)>=4 and len(approx)<=5:
                                biggest = approx
                                max_area = area
        
                                
        print("Max Area Found: " + str(max_area))
        x, y, w, h = cv2.boundingRect(biggest)
        
        if(debug==True):
            plt.figure("Dtected_Grids")
            plt.imshow(result, cmap='gray'), plt.title('I found this:')
            plt.axis("off")
            
        
        if(objective == "mask"):
            sudoku = np.zeros(edges.shape, dtype='uint8')
            sudoku[y:y+h, x:x+w]=255
            
            return sudoku
            
        elif(objective == "region"):
            
            return img[y:y+h, x:x+w]
            
            
    def resize_founded_grid_to_default(self, img, debug):
        #resize image to default size by keeping ratio
        width = 1280
        r = float(width) / img.shape[1]
        dim = (width, int(img.shape[0] * r))
        
        # perform the actual resizing of the image and show it
        resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
        
        if(debug == True):
            plt.figure("Resized_Grid_0")
            plt.imshow(resized, cmap='gray'), plt.title('Resized Grid Detected')
            plt.axis("off")
        
        return resized
            