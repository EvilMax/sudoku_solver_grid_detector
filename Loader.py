#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 25 09:35:26 2016
#20101995793950
@author: max
"""
import cv2
import numpy as np
from Shared_Functions import *


class Loader:

    def __init__(self, img):

        self.image = self.load_image(img)

        self.gray = self.change_to_gray_cahnnel(self.image)

        self.size = self.get_img_size(self.gray)

        self.gray = self.avoid_reflective_surfaces(self.gray)

        blurred = cv2.GaussianBlur(self.gray, (5, 5), 0)
        
        # from Support functions
        self.edges = apply_canny_treshold(blurred)

        self.edges = self.adjust_a_bit_with_morfology(self.edges)
        
        #can ruin programm working
        #self.edges = improve_obtained_grid_Hough(self.edges, tollerance=520, line_width=5)



    def load_image(self, img):

        img = cv2.imread(img.strip())

        img = resize_to_default(img)

        return (img)


    def change_to_gray_cahnnel(self, img):

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        return (gray)


    def get_img_size(self, img):

        return (img.shape)


    def avoid_reflective_surfaces(self, img):

        # create a CLAHE object (Arguments are optional).
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        img = clahe.apply(img)

        return (img)


    def adjust_a_bit_with_morfology(self, thresh):
        kernel = np.ones((5, 5), np.uint8)
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

        return (thresh)
