#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 14:57:07 2017

@author: max
"""
from sklearn.externals import joblib


class Learn:
    
    def __init__(self, labels, features, classifier, debug=False):
        
        classifier.fit(features, labels)
        
        # save trained classifier
        joblib.dump(classifier, "personal_knn_32.pkl", compress=3)