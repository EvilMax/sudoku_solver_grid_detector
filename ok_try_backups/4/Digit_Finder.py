#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  1 10:49:49 2017

@author: max
"""

import cv2
import os
import numpy as np
from sklearn.externals import joblib
from skimage.feature import hog
from matplotlib import pyplot as plt

class Digit_Finder:
    
    def __init__(self, grid, tailed_grid, classifier_path, grid_size, image_name, debug=False):
        
        self.result = []
        self.string_for_recognition = []
        self.extracted_features = []
        #gray = cv2.cvtColor(grid, cv2.COLOR_BGR2GRAY)
        
        # load classifier
        self.classifier = joblib.load(classifier_path)
        
        #83,164
        counter = 0
        for i in range((grid_size+2), ((grid_size*2)+2)):
            
            tmp = tailed_grid.copy()
            
            #transform to mask
            tmp[tmp==i] = 255
            tmp[tmp!=255] = 0
            
            
            num = cv2.bitwise_and(grid, grid, mask=tmp.astype('uint8'))
            
            counter += 1
            num, features = self.recognize_num(self.classifier, num, counter, tmp.astype('uint8'), image_name, debug)
            
            self.extracted_features.append(features)
            self.result.append(num)
            
            if(debug):
                print(num)
            
            if(num == " "):
                self.string_for_recognition.append("0")
            else:
                self.string_for_recognition.append(num)
        
        directory = "./output/contents/"+image_name
        if not os.path.exists(directory):
            os.makedirs(directory)
        # write to file
        #they are needed just to save results, newer reloaded inside script
        with open(directory+"/recognized_as_for_project.txt", "w") as file:
            for res in self.result:
                file.write(res)
                
        with open(directory+"/recognized_as_for_detector.txt", "w") as file:
            for res in self.string_for_recognition:
                file.write(res)
          
        
        
    def recognize_num(self, classifier, img, counter, mask, image_name, debug):
        #20101995793950
#        if(counter==24):
#            plt.figure(1100)
#            plt.imshow(img, cmap='gray')

        
        _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
        if(contours is None):
            print("An error occured in script. \n Contour is empty! Source: Digit_Finder")
            return
        
        x, y, w, h = cv2.boundingRect(contours[0])
        img = img[y:y+h,x:x+w]
        
        img_backup = img.copy()
        
        
        img = cv2.GaussianBlur(img, (5, 5), 10)
        _, img = cv2.threshold(img,75,255,cv2.THRESH_BINARY)
#        img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
#            cv2.THRESH_BINARY,11,2)
        img = 255-img
        
        #img = cv2.erode(img, (5, 5), iterations=5)
        
        
#        backup = None
#        num_labels=1
        #20101995793950
#        iterations = 0
#        while(num_labels==1 and iterations<=10):
#            iterations += 1
#            
#            print("erosion")
#        
#            backup = img.copy()
#            
#            
#            img = cv2.erode(img, (5, 5), iterations=1)
#            
#            #erode until its needed
#            output = cv2.connectedComponentsWithStats(img, 4, cv2.CV_32S)
#            # The first cell is the number of labels
#            # 0 are also labelled so: -1
#            num_labels = (output[0]-1)
#        
#            # use last good result
#        img = backup
        
        
        
        roi = cv2.resize(img, (50, 50), interpolation=cv2.INTER_AREA)
        roi = cv2.GaussianBlur(roi, (5, 5), 0)
        
        roi_backup = roi.copy()
        # estimate quantity of white pixels
        
        #blur more to eliminate details
        roi = cv2.medianBlur(roi, 15)
        
#        if(counter==33):
#            plt.figure(111)
#            plt.imshow(roi_backup, cmap='gray')
        
        roi[roi!=0] = 1
        whiteness = int(np.sum(roi))
        wh_rate = round((whiteness / (w*h)), 4)
        
        if(debug):
            print("Whiness"+str(counter)+": "+str(wh_rate)+" White: "+str(whiteness)+" Size: "+str(w*h))
        
        
        
#        if(counter==15):
#            plt.figure(111)
#            plt.imshow(roi_backup, cmap='gray')
            
        # write each non empty cell
        directory = "./output/images/"+image_name
        if not os.path.exists(directory):
            os.makedirs(directory)
        
            
        dest_dir =  directory+"/"+str(counter)+".png"
        roi_to_write = cv2.cvtColor(img_backup, cv2.COLOR_GRAY2RGB)
        cv2.imwrite(dest_dir, roi_to_write)
        
        #i will may need them in future, even for empty cells
        # calculate HOG features
        roi_hog_fd = hog(roi_backup, orientations=9, pixels_per_cell=(25, 25), cells_per_block=(1, 1), visualise = False)
        
        # if whiteness is too low so even dont disturb classifier, i already know the answer
        if(wh_rate < 0.0031):
            return ( [" ", roi_hog_fd])
            
        else:
            
            roi_hog_fd_tmp = np.array([roi_hog_fd], 'float32')
            nbr = classifier.predict(roi_hog_fd_tmp)
            
            return ( [str(nbr[0]), roi_hog_fd] )  
        
    
    