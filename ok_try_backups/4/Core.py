#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 14:56:45 2016

@author: max
"""
# just to clear workspace in Spyder3
#from IPython import get_ipython
#ipython = get_ipython()
#ipython.magic("%reset -f")
###################################

import subprocess

import cv2
import os
import numpy as np
import colorama
from colorama import Fore, Back, Style
from matplotlib import pyplot as plt

from Loader import *
from Grid_Detector import *
from Deeper_Grid_Analyzer import *
from Digit_Finder import *
from Learn import *
from Draw_Solution import *

# for windows console
# autoreset needed to stop color propagation
colorama.init(autoreset=True)

class Core:
    
    def __init__(self, img_path, classifier_path, GRID_SIZE, ENABLE_SELF_IMPROVEMENT, DEBUG, VISUAL_DEBUG):
        
        #some gloabal vars
        self.classifier = None
        image_name = os.path.splitext( os.path.basename(img_path) )[0]
        solution = None
        
        print(Back.LIGHTCYAN_EX +"ANALYZING IMAGE: "+image_name+" please be patien")
        
        # PREPROCESSING
        # load image and do first standard steps
        #img = Loader("./images/20161128_115306.jpg")
        img = Loader(img_path.strip())
        
        # used to detect mask and region
        detected_grid = Grid_Detector(img.gray, img.edges, GRID_SIZE)
        
        
        #advanced_analyze = None
        if(detected_grid.grid_found[0] is None):
            print("I wasnt able to find global bounding box around grid, so its usless to go on")
            self.done_successfully = False
            advanced_analyze=None
            
        elif(detected_grid.done == True):
        #20101995793950    
        #    grid=detected_grid.grid
        #    
        #    tmp = detected_grid.labels.copy()
        #    
        #    #transform to mask
        #    tmp[tmp==83] = 255
        #    tmp[tmp!=255] = 0
            
            digits = Digit_Finder(detected_grid.grid, detected_grid.labels, classifier_path, GRID_SIZE, image_name, DEBUG)
            self.done_successfully=True
            advanced_analyze=None
            #print("dd")
            
        elif(detected_grid.done == False):
            advanced_analyze = Deeper_Grid_Analyzer(detected_grid.grid, GRID_SIZE)
            
            # find digits after deeper analyzing
            if(advanced_analyze.done == False):
                print("SORRY; BUT I WASNT ABLE TO FIND GRID CORRECTLY! \nTry to get better photo")
                self.done_successfully=False
            else:
                digits = Digit_Finder(detected_grid.grid, advanced_analyze.labels, classifier_path, GRID_SIZE, image_name, DEBUG)
                self.done_successfully=True
        
                
                
        
        # if we found a grid correctly, do....
        if( not(detected_grid.grid_found[0] is None) and detected_grid.done == True or ( not(advanced_analyze is None) and advanced_analyze.done == True) ):
            
            #notation: N0N00N...
            parameters = "".join(digits.string_for_recognition)
            
            print("Seems i've done")
            print(Back.LIGHTRED_EX + "Your input were:")
            print(Fore.BLUE + parameters)
            
            
            if(ENABLE_SELF_IMPROVEMENT == True):
                
                
                #the classification wasnt correct
                head, tail = os.path.split(img_path)
                descriptor = head+"/"+image_name+".txt"
                
                #for debug
                #print(descriptor)
                
                #if we have descriptor
                if( os.path.exists(descriptor) ):
                    with open(descriptor, 'r') as f:
                        corrected_params = f.readline()
                    
                    #port to inside default format: N0N0... from N N ...
                    # so that user can use freely both notations
                    corrected_params = corrected_params.replace(" ", "0")
                            
                    if(corrected_params != parameters):
                        print("I detected numbers in wrong way, but i learned the lesson, next time i will try to do better :)")
                        learn = Learn(corrected_params, parameters, digits.extracted_features, digits.classifier)
                        self.classifier = learn.classifier
                    else:
                        result = subprocess.check_output(['./external_sudoku_solver.py', parameters]).decode("utf-8")
                        print(Back.LIGHTMAGENTA_EX + "HERE IS YOUR SOLVED SUDOKU:")
                        print(Fore.GREEN +  + result)
                        print("I red your description and its same as my result :D")
                        
                        print("\n")
                        print("DONT FORGET TO...")
                        print("!CHECK SCRIPT DIRECTORY, './output/contents/', you will find your solutions there!")
                        print("\n")
                        

                        if( not(result is None) and result.strip() != ""):
                            solution = Draw_Solution(result, parameters, image_name)
                        else:
                            print("GRID DETECTED BUT SOLUTION NOT FOUND!")
                        
                else:
                
                
                    print("Please check if i collected numbers correctly...")
                    correctness = input("Everything is correct? [y/n]   ").lower()[0]
                    
                    # user said everything is right
                    # works with y, ye, yes, y..., or other letters
                    if(correctness == "y"):
                    
                        #print( str(digits.string_for_recognition) )
                        
                        result = subprocess.check_output(['./external_sudoku_solver.py', parameters]).decode("utf-8")
                        
                        print("\n")
                        print(Back.LIGHTRED_EX + "Your input were:")
                        print(Fore.BLUE + parameters)
                        print("\n")
                        print(Back.LIGHTRED_EX + "HERE IS YOUR SOLVED SUDOKU:")
                        print(Fore.GREEN + result)
                        
                        print("\n")
                        print("DONT FORGET TO...")
                        print("!CHECK SCRIPT DIRECTORY, './output/contents/', you will find your solutions there!")
                        print("\n")
                        

                        if( not(result is None) and result.strip() != ""):
                            solution = Draw_Solution(result, parameters, image_name)
                        else:
                            print("GRID DETECTED BUT SOLUTION NOT FOUND!")
                            
                        
                    else:
                        
                                
                                
                        print("Ok i'm wrong, but where? There isnt descriptor file for "+image_name+" Please correct me...")
                        print("!!!Copy, paste and edit found numbers!!!")
                
                        corrected_params = input("Pasted: ")       
                
                        while( len(corrected_params) != GRID_SIZE or (corrected_params == "".join(parameters)) ):
                            print(str(len(corrected_params)))
                            print("String must contain "+str(GRID_SIZE)+" characters, retry please!")
                            print("Type stop to see to go on anyway, CTRL+C to stop script!")
                            corrected_params = input("Pasted: ")
                            if(corrected_params == "stop"):
                                break
                        #20101995793950
                #        parameters = np.array(parameters, np.str0)
                #        corrected_params = np.array(list(corrected_params), np.str0)
                #        features = np.array(digits.extracted_features, np.float32)
                        learn = Learn(corrected_params, parameters, digits.extracted_features, digits.classifier)
                        self.classifier = learn.classifier
                        print("Ok, i will try to remember it next time :D")
                    
            else:
                # if there is no need of self improvement
                result = subprocess.check_output(['./external_sudoku_solver.py', parameters]).decode("utf-8")
                print(Back.LIGHTRED_EX + "HERE IS YOUR SOLVED SUDOKU:")
                print(Fore.GREEN + result)
                
                print("\n")
                print("DONT FORGET TO...")
                print("!CHECK SCRIPT DIRECTORY, './output/contents/', you will find your solutions there!")
                print("\n")
                

                if( not(result is None) and result.strip() != ""):
                    solution = Draw_Solution(result, parameters, image_name)
                else:
                    print("GRID DETECTED BUT SOLUTION NOT FOUND!")
                
        
        
        #just to show results
        if(VISUAL_DEBUG == True and not(detected_grid.grid_found[0] is None)):        
                
            plt.figure(1)
            plt.imshow(img.edges, cmap='gray')
            
            plt.figure(2)
            plt.imshow(detected_grid.img_analysis_result, cmap='gray')
            
            plt.figure(3)
            plt.imshow(detected_grid.grid_mask, cmap='gray')
            
            plt.figure(4)
            plt.imshow(detected_grid.labels, cmap='gray')
            
            plt.figure(5)
            plt.imshow(detected_grid.result)
            
            plt.figure(6)
            plt.imshow(detected_grid.grid, cmap='gray')
            
            if( not(solution is None) and detected_grid.done == True or ( not(advanced_analyze is None) and advanced_analyze.done == True) ):
                plt.figure("final")
                plt.imshow(solution.output)
                plt.axis("off")   
            
            
            if(detected_grid.done == False):
            
                plt.figure(7)
                plt.imshow(advanced_analyze.result, cmap='gray')
                
                #a = advanced_analyze.labels
                
                plt.figure(8)
                plt.imshow(advanced_analyze.labels, cmap='gray')
            
            # to show all results, even from console
            plt.show()