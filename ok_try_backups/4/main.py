#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 14:56:45 2016

@author: max
"""
import argparse
import os
import glob
import colorama
from colorama import Fore, Back, Style
from datetime import datetime
from sklearn.externals import joblib
from Core import *


# needed for Windows, no effect on other consoles
colorama.init(autoreset=True)


parser = argparse.ArgumentParser()

parser.add_argument("--ai", help="Turn on AI (--ai), Default=generally false, the program becomes more interactive, and tries to learn from user if there is something to learn.", action="store_true")
parser.add_argument("--debug", help="Show some processing steps, (--debug) to enable", action="store_true")
parser.add_argument("--i", help="Analyze custom image, write full path, or relative on linux shell ./...", type=str)
parser.add_argument("--f", help="Analyze custom images from folder, write full path, or relative on linux shell ./...", type=str)
parser.add_argument("--custom_classifier", help="Load custom classifier, write full path, or relative on linux shell ./...", type=str)
parser.add_argument("--break_lines", help="If you load custom classifier you may want to break result in lines for future processing", action="store_true")
parser.add_argument("--visual_debug", help="If you load custom image, you may display visually some steps, its not useful, and can eat much memory if you are solving multiple images at the same time", action="store_true")
parser.add_argument("--img_extension", help="By default i read jpg file, but you can change extension here, useful, if you read a folder with png or other image types. Usage: --img_extension png (dont use .)", type=str)

args = parser.parse_args()

# starting time
global_time = datetime.now()

###################################
###################################
###################################
#Must have variables
GRID_SIZE=81 #9x9

if( not(args.custom_classifier is None) ):
    
    classifier_path = args.custom_classifier
    if(args.ai):
        ENABLE_SELF_IMPROVEMENT = True
    else:
        ENABLE_SELF_IMPROVEMENT = False
    
else:
    
    if(args.ai):
        ENABLE_SELF_IMPROVEMENT = True
        # load SGDClassifier that can learn each time
        classifier_path = "./classifiers/personal_sgd_32.pkl"
    else:
        ENABLE_SELF_IMPROVEMENT = False
        # load static KNN classifier
        classifier_path = "./classifiers/personal_knn_32.pkl"


if(args.debug):
    DEBUG = True
else:
    DEBUG = False
    
if(args.visual_debug):
    VISUAL_DEBUG = True
else:
    VISUAL_DEBUG = False
    
if( not(args.img_extension is None) ):
    EXT = args.img_extension.strip()
else:
    EXT = "jpg"
###################################
###################################
#20101995793950
###################################
core = None

#actual execution call
if( not(args.i is None) ):
    
    if not os.path.exists(args.i):
        print("File doesnt exists!")
    else:
        startTime = datetime.now()
        core = Core(args.i, classifier_path, GRID_SIZE, ENABLE_SELF_IMPROVEMENT, DEBUG, VISUAL_DEBUG)
        print(Back.CYAN + "SOLVED "+args.i+" in: "+str(datetime.now() - startTime))
        

elif(  not(args.f is None) ):
    
    if not os.path.exists(args.f):
        print("Directory doesnt exists!")
        
    else:
        
        detected = []
        undetected = []
        # default EXT = jpg
        for f in sorted(glob.glob(args.f+"*."+EXT)):
            
            startTime = datetime.now()
            core = Core(f, classifier_path, GRID_SIZE, ENABLE_SELF_IMPROVEMENT, DEBUG, VISUAL_DEBUG)
            print(Back.CYAN + "SOLVED "+f+" in: "+str(datetime.now() - startTime))
            
            if(core.done_successfully == True):
                detected.append(f)
            else:
                undetected.append(f)
        
        # remember wich images i recognized and wich not
        with open('./output/recognized_images.txt', 'w') as file:
            for i in detected:
                file.write(i+"\n")
                
        with open('./output/undetected_images.txt', 'w') as file:
            for i in undetected:
                file.write(i+"\n")
                
else:
    #default
    img_path = "./images/first.jpg"
    
    if not os.path.exists(img_path):
        print("USE --help to get help and specify target image!")
    else:
        startTime = datetime.now()
        core = Core(img_path, classifier_path, GRID_SIZE, ENABLE_SELF_IMPROVEMENT, DEBUG, VISUAL_DEBUG)
        print(Back.CYAN + "SOLVED "+img_path+" in: "+str(datetime.now() - startTime))


# save classifier if we are using AI flag, so next time machine will be cleverer
#20101995793950
# mostly it avoids to save multiple times the classifier during folder analysys    
if( not(core is None) and not(core.classifier is None) and (args.ai) ):
    # save trained classifier
    #override over existent to be more intelligent next time
    joblib.dump(classifier, classifier_path, compress=3)
    
    
print(Back.GREEN + "TOTAL SOLVING TIME: " + str(datetime.now() - global_time))