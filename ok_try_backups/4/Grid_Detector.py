#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 25 14:59:26 2016

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
import math
from Shared_Functions import *

class Grid_Detector:

    def __init__(self, img, edges, grid_size, debug=False):

        self.grid_found = self.find_contours(img, edges, debug)
        if(self.grid_found[0] is None):
            return
        
        # try to wrap correctly founded grid
        #self.grid, self.grid_mask = self.wrap_perspecive(self.grid_found[0], self.grid_found[1], self.grid_found[2])
            
            
        self.grid = resize_to_default(self.grid_found[0], debug)

        self.grid_mask = resize_to_default(self.grid_found[1], debug)
        
        #self.grid_mask_backup = self.grid_mask
        
        self.grid_Bounding = self.grid_found[2]
        self.img_analysis_result = self.grid_found[3]
        
        
        rotated = self.adjust_rotation_2(self.grid, self.grid_mask, self.grid_Bounding)
        
        #sometimes not work properly
        self.grid = rotated[0]
        self.grid_mask = rotated[1]
        self.angle = rotated[2]
        
        #blur mask a bit to avoid more false positives
        self.grid_mask = cv2.GaussianBlur(self.grid_mask, (9,9), 0)
        
        ##############################
        ## LETS FIND ACTUALLY THE GRID
        ##############################
        self.grid_mask = improve_obtained_grid_Hough(self.grid_mask, tollerance=1200)
        
        self.labels, self.result, self.num_labels = find_grid_cells(self.grid_mask)
        
        #+1 is background label
        if(self.num_labels < (grid_size+1)):
            print("Need more complex analysis!!")
            self.done = False
            
        else:
            # not all were relabeled
            # becouse i'm numbering from 83 to 163
            if(np.max(self.labels) < ((grid_size*2)+1)):
                # try to rotate this time
                height, width = self.grid.shape
                #lets rotate
                M = cv2.getRotationMatrix2D((width/2,height/2), -self.angle, 1)
                
                grid = cv2.warpAffine(self.grid, M, (width, height))
                grid_mask = cv2.warpAffine(self.grid_mask, M, (width, height))
    
                
                labels, result, num_labels = find_grid_cells(grid_mask)
                
                if(num_labels < (grid_size+1)):
                    print("Need more complex analysis!!")
                    self.done = False
                    
                else:
                    self.grid = grid
                    self.grid_mask = grid_mask
                    self.labels = labels
                    self.result = result
                    self.num_labels = num_labels
                    print("! Seems we have,  Successfully detected grid !")
                    self.done = True
                    
            else:
                print("! Seems we have,  Successfully detected grid !")
                self.done = True

        
        
    def find_contours(self, img, edges, debug, call=1):

        height, width = edges.shape

        _, contours, _ = cv2.findContours(edges.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        result = cv2.cvtColor(edges.copy(), cv2.COLOR_GRAY2RGB)


        biggest = None
        max_area = 0
        for cnt in contours:
                area = cv2.contourArea(cnt)
                if area > width:
                        epsilon = 0.05 * cv2.arcLength(cnt, True)
                        approx = cv2.approxPolyDP(cnt, epsilon, True)



                        # draw also rectangle to save time:
                        if area > (width*height)/100 and len(approx) >= 4 and len(approx) <= 8:
                            x, y, w, h = cv2.boundingRect(cnt)
                    
                            #take just only things similar to squares
                            ar = w / float(h)

                            if(ar >= 0.9 and ar <= 1.2):
                                
                                cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)
                                
                                # update maximum rectangle found
                                # if its a quad +-
                                if area > max_area:
                                    
                                    biggest = approx
                                    max_area = area

        
        if(debug==True):
            print("Max Area Found: " + str(max_area))
            
            plt.figure("Dtected_Grids")
            plt.imshow(result, cmap='gray'), plt.title('I found this:')
            plt.axis("off")
        
        #try to find global box
        if(biggest is None):
            if(call==1):
                #improve with hough lines already at start   
                edges = improve_obtained_grid_Hough(edges, tollerance=450)
                
    #            plt.figure(994)
    #            plt.imshow(edges, cmap='gray')
                print("I even tried to improve grid before first detection")
                grid, grid_mask, biggest, result = self.find_contours(img, edges, debug, call=2)
                
    #            plt.figure(995)
    #            plt.imshow(grid_mask, cmap='gray')
                
                return [grid, grid_mask, biggest, result]
            else:
                return [None, None, None, None]
            
        else:
            x, y, w, h = cv2.boundingRect(biggest)
            #create crops
            grid = img[y:y+h, x:x+w]
            grid_mask = edges[y:y+h, x:x+w]
            #20101995793950
# CAN DELETE            
#            #lets do it with minimum area rectangle
#            rect = cv2.minAreaRect(biggest)
#            box = cv2.boxPoints(rect)
#            box = np.int32(box)
#            
#            print(str(box))
#            
#            mask = np.zeros(img.shape, dtype=np.uint8)
#            roi_corners = np.array([[(box[0]), (box[1]), (box[2]), (box[3])]], dtype=np.int32)
#            
#            # fill the ROI so it doesn't get wiped out when the mask is applied
#            cv2.fillPoly(mask, roi_corners, 255)
#            
#            # apply the mask
#            grid = cv2.bitwise_and(img, mask)
#            grid_mask = cv2.bitwise_and(edges, mask)
#            
#            pts1 = np.float32([[box[0][0], box[0][1]],[box[1][0], box[1][1]],[box[2][0], box[2][1]],[box[3][0], box[3][1]]])
#            pts2 = np.float32([[0, 0],[0, width],[height, 0],[width, height]])
#            
#            M = cv2.getPerspectiveTransform(pts1,pts2)
#            dst = cv2.warpPerspective(img,M,(width,height))
#            
#            # save the result
#            cv2.imwrite('./image_masked.png', dst)
            
            
            return [grid, grid_mask, biggest, result]

        
    def adjust_rotation_2(self, grid, grid_mask, bounding):
        
        height, width = grid.shape
        
        minRect = cv2.minAreaRect(bounding)
        box = cv2.boxPoints(minRect)
        box = np.int0(box)
        
        # compute rotation angle
        angle = minRect[2]
        edge1 = np.array( (box[1][0], box[1][1]) ) - np.array( (box[0][0], box[0][1]) )
        edge2 = np.array( (box[2][0], box[2][1]) ) - np.array( (box[1][0], box[1][1]) )
        
        #select biggest edge to compute angle
        used_edge = edge2
        if(cv2.norm(edge2) > cv2.norm(edge1)):
            used_edge = edge2
            
        #horizontal line to calculate rotation angle
        reference = np.array( (1, 0) )
        
        #print(str(edge1))
        
        #compute correctly angle of rotation
        angle = 180.0/math.pi * math.acos( 
                                          (reference[0] * used_edge[0] + reference[1] * used_edge[1]) /
                                          ( cv2.norm(reference) * cv2.norm(used_edge) ) 
                                         )
        
        print("Grid rotation: "+str(round(angle,2)))
        
        if( not( (abs(angle)>=85 and abs(angle)<=95) or (abs(angle)>=175 and abs(angle)<=185) or (abs(angle>=0) and abs(angle)<=6) ) ):
        #if( not( (abs(angle)>=175 and abs(angle)<=185) ) ):
            
            
            final_rotation = -round(angle)
            
            print("Will be rotated by: "+str(final_rotation)+" degrees to left!")
            
            #lets rotate
            M = cv2.getRotationMatrix2D((width/2,height/2), final_rotation, 1)
            
            grid = cv2.warpAffine(grid, M, (width, height))
            grid_mask = cv2.warpAffine(grid_mask, M, (width, height))

        return [grid, grid_mask, angle]

        #20101995793950      
#    def wrap_perspecive(self, grid, grid_mask, bounding_box):
#        # experimental code seems to work
#        #perspective transformation for irregular grids
#        x, y, w, h = cv2.boundingRect(bounding_box)
#        height, width = grid.shape
##        print(str(x))
##        print(str(y))
##        print(str(w))
##        print(str(h))
##        print(str(width))
#        origin = np.float32([(x,y), (x+w, y), (x, y+h), (x+w, y+h)])
#        dest = np.float32([(0, 0), (width, 0), (0, height), (width, height)])
#        
#        M = cv2.getPerspectiveTransform(origin, dest)
#        grid = cv2.warpPerspective(grid, M, (width, height))
#        grid_mask = cv2.warpPerspective(grid_mask, M, (width, height))
#        
#        return [grid, grid_mask]