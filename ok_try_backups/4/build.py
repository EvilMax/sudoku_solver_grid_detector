import sys
from cx_Freeze import setup, Executable

includes = []
excludes = []
packages = ['numpy', 'scipy', 'matplotlib', 'tkinter', 'cv2', 'argparse', 'os', 'glob', 'colorama', 'datetime', 'sklearn', 'skimage']
path = []

base = 'Console'

#2 = max optimization, (0,1,2)

options = {
    'build_exe': {
        "includes": includes,
        "excludes": excludes,
        "packages": packages,
        "path": path,
        "optimize": 2
        #'excludes': ['Tkinter']  # Sometimes a little finetuning is needed
    }
}

executables = [Executable('main.py', base=base)]
setup(name='main',
      version='0.1',
      description='Solve sudoku from photo!',
      executables=executables,
      options=options
      )