#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 28 18:23:32 2016

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
from Shared_Functions import *


class Deeper_Grid_Analyzer:
    
    def __init__(self, grid, grid_size, debug=False):
        
        self.is_blurred = self.detect_blurring(grid)
        
        if(self.is_blurred):
            #sharpen image using blur and unsharp method
            gaussian = cv2.GaussianBlur(grid, (9,9), 10.0)
            grid = cv2.addWeighted(grid, 1.2, gaussian, -1.0, 0, grid)
            
#            plt.figure(999)
#            plt.imshow(grid, cmap='gray')

        #self.grid = self.apply_canny_treshold(grid)
        
        #might work sometimes
        #self.grid = cv2.GaussianBlur(grid, (9, 9), 10)
        
        # from support functions
        self.grid = apply_canny_treshold(grid)
        
        kernel = np.ones((3, 3),np.uint8)
        self.grid = cv2.dilate(self.grid, kernel, iterations = 3)
        #self.grid = cv2.morphologyEx(self.grid, cv2.MORPH_CLOSE, kernel)
        
        #might work sometimes
        #self.grid = cv2.medianBlur(self.grid, 5)
       
        
        #try to search now, if not ok, so you can try to use hough
        self.labels, self.result, self.num_labels = find_grid_cells(self.grid)
        
        #82=(grid_size+1) becouse 0 is counted as label
        rate = 1200
        while(self.num_labels < (grid_size+1)):
            
            if(rate<=800):
                break
            
            #self.grid = improve_obtained_grid_Hough(self.grid, tollerance=850)
            self.grid = improve_obtained_grid_Hough(self.grid, tollerance=rate)
            #self.grid = cv2.gaussianBlur(self.grid, (5,5), 10)
            self.labels, self.result, self.num_labels = find_grid_cells(self.grid)
            
            rate -= 100

        
        #to incapsulate better the code and do it more readable to final user
        if(self.num_labels < (grid_size+1)):
            self.done = False
        else:
            self.done = True
            
        #kernel2 = np.ones((9, 9),np.uint8)
        #self.grid = cv2.erode(self.grid, kernel, iterations = 6)
        #self.grid = cv2.morphologyEx(self.grid, cv2.MORPH_OPEN, kernel2)

        
        
    
    def detect_blurring(self, img):
        
        #actually detect blur
        blur_lvl = cv2.Laplacian(img, cv2.CV_64F).var()
        
        #estimated by me
        threshold = 250
        
        if(blur_lvl < threshold):
            print("The Grid seems to be Blurred!")
            print("Blur level: "+str(round(blur_lvl, 2)))
            return True
        else:
            return False
