#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 14:56:45 2016

@author: max
"""
# just to clear workspace in Spyder3
#from IPython import get_ipython
#ipython = get_ipython()
#ipython.magic("%reset -f")
###################################

import subprocess

import cv2
import numpy as np
from matplotlib import pyplot as plt

from Loader import *
from Grid_Detector import *
from Deeper_Grid_Analyzer import *
from Digit_Finder import *
#from Learn import *
from Draw_Solution import *


#Must have variables
GRID_SIZE=81 #9x9
ENABLE_SELF_IMPROVEMENT = True

# PREPROCESSING
# load image and do first standard steps
img = Loader("./images/20161128_115306.jpg")

# used to detect mask and region
detected_grid = Grid_Detector(img.gray, img.edges, GRID_SIZE, debug=False)


#advanced_analyze = None
if(detected_grid.grid_found[0] is None):
    print("I wasnt able to find global bounding box around grid, so its usless to go on")
    
    
elif(detected_grid.done == True):
    
#    grid=detected_grid.grid
#    
#    tmp = detected_grid.labels.copy()
#    
#    #transform to mask
#    tmp[tmp==83] = 255
#    tmp[tmp!=255] = 0
    
    digits = Digit_Finder(detected_grid.grid, detected_grid.labels, GRID_SIZE)
    #print("dd")
    
elif(detected_grid.done == False):
    advanced_analyze = Deeper_Grid_Analyzer(detected_grid.grid, GRID_SIZE)
    
    # find digits after deeper analyzing
    if(advanced_analyze.done == False):
        print("SORRY; BUT I WASNT ABLE TO FIND GRID CORRECTLY! \nTry to get better photo")
    else:
        digits = Digit_Finder(detected_grid.grid, advanced_analyze.labels, GRID_SIZE)

        
#needed becouse sometimes digits seems to be executed in multithreading mode
can_solve=True

# if we found a grid correctly, do....
if( can_solve==True and detected_grid.done == True or ( not(advanced_analyze is None) and advanced_analyze.done == True) ):
    
    parameters = "".join(digits.string_for_recognition)
    
    print("Seems i've done")
    print("Your input were:")
    print(parameters)
    
    result = subprocess.check_output(['./external_sudoku_solver.py', parameters]).decode("utf-8")
    
    print("HERE IS YOUR SOLVED SUDOKU:")
    print(result)
    
    solution = Draw_Solution(result, parameters)
        

 
#        
##just to show results
#if(not(detected_grid.grid_found[0] is None)):        
#        
#    plt.figure(1)
#    plt.imshow(img.edges, cmap='gray')
#    
#    plt.figure(2)
#    plt.imshow(detected_grid.img_analysis_result, cmap='gray')
#    
#    plt.figure(3)
#    plt.imshow(detected_grid.grid_mask, cmap='gray')
#    
#    plt.figure(4)
#    plt.imshow(detected_grid.labels, cmap='gray')
#    
#    plt.figure(5)
#    plt.imshow(detected_grid.result)
#    
#    plt.figure(6)
#    plt.imshow(detected_grid.grid, cmap='gray')
#    
#    plt.figure("final")
#     plt.imshow(solution.output)
#     plt.axis("off")    
#    
#    if(detected_grid.done == False):
#    
#        plt.figure(7)
#        plt.imshow(advanced_analyze.result, cmap='gray')
#        
#        #a = advanced_analyze.labels
#        
#        plt.figure(8)
#        plt.imshow(advanced_analyze.labels, cmap='gray')