#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 25 14:59:26 2016

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
import math


class Grid_Detector:

    def __init__(self, img, edges, debug=False):

        self.grid_found = self.find_contours(img, edges, debug)

        self.grid = self.resize_founded_grid_to_default(self.grid_found[0], debug)

        self.grid_mask = self.resize_founded_grid_to_default(self.grid_found[1], debug)

        #self.grid_mask_backup = self.grid_mask
        
        self.grid_Bounding = self.grid_found[2]
        
        rotated = self.adjust_rotation_2(self.grid, self.grid_mask, self.grid_Bounding)
        
        self.grid = rotated[0]
        self.grid_mask = rotated[1]
        
        #blur mask a bit to avoid more false positives
        self.grid_mask = cv2.GaussianBlur(self.grid_mask, (9,9), 0)
        
        ##############################
        ## LETS FIND ACTUALLY THE GRID
        ##############################
        self.grid_mask = self.improve_obtained_grid(self.grid_mask)
        
        self.labels, self.result, self.num_labels = self.find_grid_cells(self.grid_mask)
        
        #+1 is background label
        if(self.num_labels < (81+1)):
            print("Need more complex analysis!!")
        else:
            print("! Seems we have,  Successfully detected grid !")
        
        
    def find_contours(self, img, edges, debug):

        height, width = edges.shape

        _, contours, _ = cv2.findContours(edges.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        result = cv2.cvtColor(edges.copy(), cv2.COLOR_GRAY2RGB)


        biggest = None
        max_area = 0
        for cnt in contours:
                area = cv2.contourArea(cnt)
                if area > width:
                        epsilon = 0.05 * cv2.arcLength(cnt, True)
                        approx = cv2.approxPolyDP(cnt, epsilon, True)



                        # draw also rectangle to save time:
                        if area > width and len(approx) >= 4 and len(approx) <= 8:
                            x, y, w, h = cv2.boundingRect(cnt)
                    
                            #take just only things similar to squares
                            ar = w / float(h)

                            if(ar >= 0.9 and ar <= 1.2):
                                
                                cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)
                                
                                # update maximum rectangle found
                                # if its a quad +-
                                if area > max_area:
                                    
                                    biggest = approx
                                    max_area = area

        
        if(debug==True):
            print("Max Area Found: " + str(max_area))
            
            plt.figure("Dtected_Grids")
            plt.imshow(result, cmap='gray'), plt.title('I found this:')
            plt.axis("off")
        
        x, y, w, h = cv2.boundingRect(biggest)
        #create crops
        grid = img[y:y+h, x:x+w]
        grid_mask = edges[y:y+h, x:x+w]
        
        return [grid, grid_mask, biggest]
            
            
    def resize_founded_grid_to_default(self, img, debug):
        #resize image to default size by keeping ratio
        width = 1280
        r = float(width) / img.shape[1]
        dim = (width, int(img.shape[0] * r))
        
        # perform the actual resizing of the image and show it
        resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
        
        if(debug == True):
            plt.figure("Resized_Grid_0")
            plt.imshow(resized, cmap='gray'), plt.title('Resized Grid Detected')
            plt.axis("off")
        
        return resized

        
    def adjust_rotation_2(self, grid, grid_mask, bounding):
        
        height, width = grid.shape
        
        minRect = cv2.minAreaRect(bounding)
        box = cv2.boxPoints(minRect)
        box = np.int0(box)
        
        # compute rotation angle
        angle = minRect[2]
        edge1 = np.array( (box[1][0], box[1][1]) ) - np.array( (box[0][0], box[0][1]) )
        edge2 = np.array( (box[2][0], box[2][1]) ) - np.array( (box[1][0], box[1][1]) )
        
        #select biggest edge to compute angle
        used_edge = edge1
        if(cv2.norm(edge2) > cv2.norm(edge1)):
            used_edge = edge2
            
        #horizontal line to calculate rotation angle
        reference = np.array( (1, 0) )
        
        #print(str(edge1))
        
        #compute correctly angle of rotation
        angle = 180.0/math.pi * math.acos( 
                                          (reference[0] * used_edge[0] + reference[1] * used_edge[1]) /
                                          ( cv2.norm(reference) * cv2.norm(used_edge) ) 
                                         )
        
        print("Grid rotation: "+str(round(angle,2)))
        
        #if( not( (abs(angle)>=85 and abs(angle)<=95) or (abs(angle)>=175 and abs(angle)<=185) ) ):
        if( not( (abs(angle)>=175 and abs(angle)<=185) ) ):
            
            
            final_rotation = -round(angle)+180
            
            print("Will be rotated by: "+str(final_rotation)+" degrees to left!")
            
            #lets rotate
            M = cv2.getRotationMatrix2D((width/2,height/2), final_rotation, 1)
            
            grid = cv2.warpAffine(grid, M, (width, height))
            grid_mask = cv2.warpAffine(grid_mask, M, (width, height))

        return [grid, grid_mask]
        
        
    def improve_obtained_grid(self, grid):
        #fix lines
        lines= cv2.HoughLines(grid.copy(), 1, math.pi/180.0, 850, np.array([]), 0, 0)
        
        a,b,c = lines.shape
        for i in range(a):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0, y0 = a*rho, b*rho
            pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
            pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
            cv2.line(grid, pt1, pt2, (255, 255, 255), 5, cv2.LINE_AA)

        return (grid)

        
    def find_grid_cells(self, grid_mask):
        
        _, contours, _ = cv2.findContours(grid_mask.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
        #create new array for future ordering
        grid_labels = np.zeros( grid_mask.shape, dtype='uint8' )
        
        #i will draw here
        result = cv2.cvtColor(grid_mask.copy(), cv2.COLOR_GRAY2RGB)
        
        
        width, height = grid_mask.shape

        counter = 0
        
        
        minArea = (width*height)/200
        maxArea = (width*height)/50
        
        for cnt in contours:
            area = cv2.contourArea(cnt)
            if area > width:
                    epsilon = 0.05 * cv2.arcLength(cnt, True)
                    approx = cv2.approxPolyDP(cnt, epsilon, True)
                    
               
                    #draw also rectangle to save time:
                    if area > minArea and area < maxArea and len(approx)>=4 and len(approx)<=8:
                    #if area > width and len(approx)>=4 and len(approx)<=8:
                      
                        
                        x, y, w, h = cv2.boundingRect(cnt)
                        
                        #take just only things similar to squares
                        ar = w / float(h)
                        
                        if(ar >= 0.85 and ar <= 1.2):
                            counter = counter + 1
                    
                            cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)
                                                    
                            # add also some text
                            # Write some Text
                            font = cv2.FONT_HERSHEY_SIMPLEX
                            cv2.putText(result, str(counter),(x+10,y+20), font, 2,(255,255,255),2)
                            
                            #fill correctly grid labels with white spaces
                            grid_labels[y:y+h, x:x+w] = 255
        
        #CONNECTED COMPONENTS LABELLING
        # You need to choose 4 or 8 for connectivity type
        connectivity = 4  
        # Perform the operation
        output = cv2.connectedComponentsWithStats(grid_labels.copy(), connectivity, cv2.CV_32S)
        # Get the results
        # The first cell is the number of labels
        num_labels = output[0]
        # The second cell is the label matrix
        labels = output[1]
        # The third cell is the stat matrix
        stats = output[2]
        # The fourth cell is the centroid matrix
        centroids = output[3]
        
        
        
        #better algorithm
        step=20
        
        need_change = True
        counter = num_labels+1
        first=True
        for i in range(0, width, step):
            #old_counter=counter
            need_change=True
            first=True
            for j in range(0, height, step):
                
                #debug, but breaks result
                #cv2.circle(labels,(i,j), 10, (0,0,0), -1)
                
                if(i>120 and j>j*step and first==True):
                    cv2.circle(labels,(i,j), 10, (255,255,255), -1)
                    need_change=False
                    first=False
                
                if(labels[i][j] != 0 and need_change==True and not(labels[i][j]>=(num_labels+1) and labels[i][j]<=counter )):
                    labels[labels==labels[i][j]] = counter
                    counter += 1
                    need_change = True
                    first=False
                    #cv2.circle(labels,(i,j), 10, (0,0,0), -1)
                    
                    
        return [labels, result, num_labels]