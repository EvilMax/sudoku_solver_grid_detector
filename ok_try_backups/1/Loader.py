#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 25 09:35:26 2016

@author: max
"""
import cv2
import numpy as np


class Loader:

    def __init__(self, img):

        self.image = self.load_image(img)

        self.gray = self.change_to_gray_cahnnel(self.image)

        self.size = self.get_img_size(self.gray)

        self.gray = self.avoid_reflective_surfaces(self.gray)

        blurred = cv2.GaussianBlur(self.gray, (5, 5), 0)

        self.edges = self.apply_canny_treshold(blurred)

        self.edges = self.adjust_a_bit_with_morfology(self.edges)


        
    def load_image(self, img):

        img = cv2.imread(img.strip())

        # resize to default width, by keeping height ratio
        width = 1280
        ratio = float(width) / img.shape[1]
        dim = (width, int(img.shape[0] * ratio))

        # perform the actual resizing of the image and show it
        # INTER_AREA gives better result compared to LINEAR
        img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

        return (img)


    def change_to_gray_cahnnel(self, img):

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        return (gray)


    def get_img_size(self, img):

        return (img.shape)


    def avoid_reflective_surfaces(self, img):

        # create a CLAHE object (Arguments are optional).
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        img = clahe.apply(img)

        return (img)


    def apply_canny_treshold(self, img, sigma=0.33):

        # compute the median of the single channel pixel intensities
        v = np.median(img)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(img, lower, upper)

        # return the edged image
        return (edged)


    def adjust_a_bit_with_morfology(self, thresh):
        kernel = np.ones((5, 5), np.uint8)
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

        return (thresh)

    def improve_grid_with_Probab_Hough(self, grid):
        minLineLength = 50
        maxLineGap = 1
        lines = cv2.HoughLinesP(
                                grid.copy(),
                                1,
                                np.pi/180,
                                10,
                                minLineLength,
                                maxLineGap
                                )
        for x in range(0, len(lines)):
            for x1, y1, x2, y2 in lines[x]:
                cv2.line(grid, (x1, y1), (x2, y2), (255, 255, 255), 4)

        return (grid)
