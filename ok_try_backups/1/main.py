#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 14:56:45 2016

@author: max
"""
# just to clear workspace in Spyder3
#from IPython import get_ipython
#ipython = get_ipython()
#ipython.magic("%reset -f")
###################################


import cv2
import numpy as np
from matplotlib import pyplot as plt

from Loader import *
from Grid_Detector import *
from Deeper_Grid_Analyzer import *

# PREPROCESSING
# load image and do first standard steps
img = Loader("./images/8a.jpg")

# used to detect mask and region
detected_grid = Grid_Detector(img.gray, img.edges, debug=False)


advanced_analyze = None
if(detected_grid.num_labels < 82):
    advanced_analyze = Deeper_Grid_Analyzer(detected_grid.grid)



#plt.figure(1)
#plt.imshow(detected_grid.grid, cmap='gray')
#
#plt.figure(2)
#plt.imshow(detected_grid.grid_mask, cmap='gray')

#plt.figure(3)
#plt.imshow(detected_grid.labels, cmap='gray')
#
#plt.figure(4)
#plt.imshow(detected_grid.result)

#plt.figure(5)
#plt.imshow(detected_grid.grid, cmap='gray')

plt.figure(6)
plt.imshow(advanced_analyze.result, cmap='gray')

#a = advanced_analyze.labels

plt.figure(7)
plt.imshow(advanced_analyze.labels, cmap='gray')