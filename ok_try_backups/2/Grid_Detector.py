#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 25 14:59:26 2016

@author: max
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
import math
from Shared_Functions import *

class Grid_Detector:

    def __init__(self, img, edges, debug=False):

        self.grid_found = self.find_contours(img, edges, debug)

        self.grid = resize_to_default(self.grid_found[0], debug)

        self.grid_mask = resize_to_default(self.grid_found[1], debug)

        #self.grid_mask_backup = self.grid_mask
        
        self.grid_Bounding = self.grid_found[2]
        self.img_analysis_result = self.grid_found[3]
        
        rotated = self.adjust_rotation_2(self.grid, self.grid_mask, self.grid_Bounding)
        
        self.grid = rotated[0]
        self.grid_mask = rotated[1]
        
        #blur mask a bit to avoid more false positives
        self.grid_mask = cv2.GaussianBlur(self.grid_mask, (9,9), 0)
        
        ##############################
        ## LETS FIND ACTUALLY THE GRID
        ##############################
        self.grid_mask = improve_obtained_grid_Hough(self.grid_mask, tollerance=1200)
        
        self.labels, self.result, self.num_labels = find_grid_cells(self.grid_mask)
        
        #+1 is background label
        if(self.num_labels < (81+1)):
            print("Need more complex analysis!!")
            self.done = False
        else:
            print("! Seems we have,  Successfully detected grid !")
            self.done = True
        
        
    def find_contours(self, img, edges, debug):

        height, width = edges.shape

        _, contours, _ = cv2.findContours(edges.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        result = cv2.cvtColor(edges.copy(), cv2.COLOR_GRAY2RGB)


        biggest = None
        max_area = 0
        for cnt in contours:
                area = cv2.contourArea(cnt)
                if area > width:
                        epsilon = 0.05 * cv2.arcLength(cnt, True)
                        approx = cv2.approxPolyDP(cnt, epsilon, True)



                        # draw also rectangle to save time:
                        if area > width and len(approx) >= 4 and len(approx) <= 8:
                            x, y, w, h = cv2.boundingRect(cnt)
                    
                            #take just only things similar to squares
                            ar = w / float(h)

                            if(ar >= 0.9 and ar <= 1.2):
                                
                                cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)
                                
                                # update maximum rectangle found
                                # if its a quad +-
                                if area > max_area:
                                    
                                    biggest = approx
                                    max_area = area

        
        if(debug==True):
            print("Max Area Found: " + str(max_area))
            
            plt.figure("Dtected_Grids")
            plt.imshow(result, cmap='gray'), plt.title('I found this:')
            plt.axis("off")
        
        x, y, w, h = cv2.boundingRect(biggest)
        #create crops
        grid = img[y:y+h, x:x+w]
        grid_mask = edges[y:y+h, x:x+w]
        
        return [grid, grid_mask, biggest, result]

        
    def adjust_rotation_2(self, grid, grid_mask, bounding):
        
        height, width = grid.shape
        
        minRect = cv2.minAreaRect(bounding)
        box = cv2.boxPoints(minRect)
        box = np.int0(box)
        
        # compute rotation angle
        angle = minRect[2]
        edge1 = np.array( (box[1][0], box[1][1]) ) - np.array( (box[0][0], box[0][1]) )
        edge2 = np.array( (box[2][0], box[2][1]) ) - np.array( (box[1][0], box[1][1]) )
        
        #select biggest edge to compute angle
        used_edge = edge2
        if(cv2.norm(edge2) > cv2.norm(edge1)):
            used_edge = edge2
            
        #horizontal line to calculate rotation angle
        reference = np.array( (1, 0) )
        
        #print(str(edge1))
        
        #compute correctly angle of rotation
        angle = 180.0/math.pi * math.acos( 
                                          (reference[0] * used_edge[0] + reference[1] * used_edge[1]) /
                                          ( cv2.norm(reference) * cv2.norm(used_edge) ) 
                                         )
        
        print("Grid rotation: "+str(round(angle,2)))
        
        if( not( (abs(angle)>=85 and abs(angle)<=95) or (abs(angle)>=175 and abs(angle)<=185) ) ):
        #if( not( (abs(angle)>=175 and abs(angle)<=185) ) ):
            
            
            final_rotation = -round(angle)
            
            print("Will be rotated by: "+str(final_rotation)+" degrees to left!")
            
            #lets rotate
            M = cv2.getRotationMatrix2D((width/2,height/2), final_rotation, 1)
            
            grid = cv2.warpAffine(grid, M, (width, height))
            grid_mask = cv2.warpAffine(grid_mask, M, (width, height))

        return [grid, grid_mask]
