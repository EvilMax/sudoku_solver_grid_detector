#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 16:39:10 2017

@author: max
"""

import os
import glob
import cv2
from skimage.feature import hog


path = "./together/"
#path_len = len(path)

features = []

for f in sorted(glob.glob(path+"*.png")):
    print(f)
    im = cv2.imread(f.strip(), 0)
    
    # do same things of Digit Recognizer
    im = cv2.GaussianBlur(im, (5, 5), 10)
    _, im = cv2.threshold(im,75,255,cv2.THRESH_BINARY)
    im = 255-im
    
    
    im = cv2.resize(im, (50, 50), interpolation=cv2.INTER_AREA)
    im = cv2.GaussianBlur(im, (5, 5), 0)
    
    # calculate HOG features
    roi_hog_fd = hog(im, orientations=9, pixels_per_cell=(25, 25), cells_per_block=(1, 1), visualise = False)
    features.append(roi_hog_fd)

#compact features    
features_final = np.array(features, 'float32')

np.save("features_final", features_final)