#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 15:45:42 2017

@author: max
"""
import os
import glob


path = "./together/"
#path_len = len(path)

images = []

for f in sorted(glob.glob(path+"*.png")):
    
    f = f[-8:]
    
    images.append(f)
    
    print ("Current File Being Processed is: " + f)
    
with open('images.list', 'w') as file:
    for im in images:
        file.write(im+"\n")