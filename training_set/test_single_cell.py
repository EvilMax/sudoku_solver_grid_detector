#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 17:26:04 2017

@author: max
"""
import cv2
import matplotlib.pyplot as plt
import numpy as np

path = "./../output_images/1.png"

# read only gray channel
img = cv2.imread(path, 0)

img = cv2.GaussianBlur(img, (5, 5), 10)
_, img = cv2.threshold(img,75,255,cv2.THRESH_BINARY)
#        img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
#            cv2.THRESH_BINARY,11,2)
img = 255-img


roi = cv2.resize(img, (50, 50), interpolation=cv2.INTER_AREA)
roi = cv2.GaussianBlur(roi, (5, 5), 0)

roi_backup = roi.copy()
# estimate quantity of white pixels

#        #blur more to eliminate details
#        roi = cv2.medianBlur(roi, 15)
#        
#        if(counter==20):
#            plt.figure(111)
#            plt.imshow(roi_backup, cmap='gray')
#        
#        roi[roi!=0] = 1
#        whiteness = int(np.sum(roi))
#        wh_rate = round((whiteness / (w*h)), 4)
#        print("Whiness"+str(counter)+": "+str(wh_rate)+" White: "+str(whiteness)+" Size: "+str(w*h))
#        
#        
#        
##        if(counter==15):
##            plt.figure(111)
##            plt.imshow(roi_backup, cmap='gray')
#            
#        
#        
#        if(wh_rate < 0.0048):
#            return (0)
#            
#        else:
    



# calculate HOG features
roi_hog_fd = hog(roi_backup, orientations=9, pixels_per_cell=(25, 25), cells_per_block=(1, 1), visualise = False)
#nbr = classifier.predict(np.array([roi_hog_fd], 'float32'))


plt.figure(1)
plt.imshow(img, cmap='gray')

plt.figure(2)
plt.imshow(roi_backup, cmap='gray')