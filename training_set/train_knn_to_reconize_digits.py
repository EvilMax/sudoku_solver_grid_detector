#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 30 11:42:09 2016

@author: max
"""

import numpy as np
from sklearn.externals import joblib
from sklearn import datasets
from sklearn.neighbors import KNeighborsClassifier
from skimage.feature import hog

###############
# load features
features = np.load("features_final.npy")
labels = []
with open("./labels.list") as f:
    for line in f.readlines():
        labels.append(line.rstrip("\n"))
        
labels = np.array(labels, 'uint8')
        

# initialize and train classifier
knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(features, labels)

# save trained classifier
joblib.dump(knn, "personal_knn_32.pkl", compress=3)