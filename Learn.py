#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 14:57:07 2017

@author: max
"""
import numpy as np

class Learn:
    
    def __init__(self, labels, old_labels, features, classifier, debug=False):
        
        #print(len(labels))
        
#        old_labels = np.array(old_labels, np.str0)
#        labels = np.array(list(labels), np.str0)
#        features = np.array(features, np.float32)
        #20101995793950
        
        #learn only missed things
        labels_to_learn = []
        features_to_learn = []
        for i in range(0, len(labels)):
            if(labels[i] != old_labels[i]):
                labels_to_learn.append(labels[i])
                features_to_learn.append(features[i])
        
        
        labels_to_learn = np.array(labels_to_learn, np.str0)
        features_to_learn = np.array(features_to_learn, 'float32')
        
        if(labels_to_learn.size != 0 and features_to_learn.size != 0):
            classifier.partial_fit(features_to_learn, labels_to_learn)
            
            self.classifier = classifier