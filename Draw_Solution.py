#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 16:51:34 2017

@author: max
"""
import cv2
import os
import numpy as np
import matplotlib.pyplot as plt


class Draw_Solution:
    
    def __init__(self, cells, existent_cells, image_name, grid_cols=9, grid_rows=9, cell_size=100):

        width = grid_cols*cell_size
        height = grid_rows*cell_size
        
        output = np.zeros((width, height), np.uint8)
        output[output==0] = 100
        output = cv2.cvtColor(output, cv2.COLOR_GRAY2RGB)
        
        #draw lines vertical
        for i in range(0, width, cell_size):
            if((i != 0) and (i != width)):
                # Draw a diagonal white line with thickness of 5 px
                cv2.line(output, (i, 0), (i, height), (255, 255, 255), 10)
        
        #draw lines horizontal
        for j in range(0, height, cell_size):
            if((j != 0) and (j != height)):
                # Draw a diagonal white line with thickness of 5 px
                cv2.line(output, (0, j), (width, j), (255, 255, 255), 10)
        
        #draw numbers in each cell
        #20101995793950
        # becouse result is string
        cells = list(cells)
        
        font = cv2.FONT_HERSHEY_SIMPLEX
        
        row = 100
        col = 0
        pos = 0
        for i in range(1, (grid_rows+1)):
            
            for j in range(1, (grid_cols+1)):
                
                x = col+20
                y = row-17
                
                if(cells[pos] == existent_cells[pos]):
                    cv2.putText(output,cells[pos], (x, y), font, 3, (255, 204, 51), 10, cv2.LINE_AA)
                else:
                    cv2.putText(output,cells[pos], (x, y), font, 3, (102, 255, 51), 10, cv2.LINE_AA)
                
                pos += 1
                
                if(j>0 and j%grid_cols==0):
                    row += 100
                    col = 0
                else:
                    col += 100
        
                
        # write solution to file
        directory = "./output/contents/"+image_name
        if not os.path.exists(directory):
            os.makedirs(directory)

        dest_dir =  directory+"/SOLVED_SUDOKU.png"
        cv2.imwrite(dest_dir, output)
                
        self.output = output