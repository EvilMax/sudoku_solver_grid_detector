**This is a sudoku recognizer from image.**
------------------------------------------------

LIBRARIES USED:
----------------------

 - Python3.5
 - OpenCV3.*
 - Numpy
 - Matplotlib
 - Math
 - Scikit Learn (sklearn)
 - Scikit Image (skimage)
 - Subprocess
 - Argparse
 - Colorama
 - Multiprocessing
 - Random
 - Sys

**What its for?**
----------------------

 - Its made mainly for solving Sudoku directly from a photos
 - You can use it to scan entire folder of photos, or just on a single image
 - Dist archive is a compiled software, its ready to go without installing manually packages. Especially OpenCV3.* can be a problem sometimes
 - **Its can be used also to detect anything on a grid, with a bit of manual machine learning**
 - So things like Chess, Weiqi, Sudoku, and so on, even not just games, but with a little of adjustments, its able to detect on grid everything you can immagine
 - But grid must be full (with external borders)
 - Grid size can be easily changed, even can be used for non symmetrical grids


Script Options:
---------
- --i="./image/path/absolute/or/relative"
- --f="./folder/path/absolute/or/relative"
- --ai  ::Flag, can be used to enable interactive machine learning for script
- --debug  ::Flag, for debug on console
- --custom_classifier="./path/to/classifier" You can just specify your own classifier to recognize an object on grid cell (you must use classifier with labels, and labels must be string)
- --break_lines  ::Flag, needed just for output of result preferences or needs
- --visual_debug  ::Flag, its better to use it only processing single image, it eats much memory, but it will display you some of key passages to identify your grid
- --img_extension="jpg"  ::Useful, if you need process a folder with png images or other formats
- --training_only  ::Flag, useful if you want to train your own classifier, or may also be used as a bridge, to detect objects in different positions and pass output to next program that will do something else with detected objects
- --grid_size="x,y"  ::Use it to analyze custom grids x = number of columns, y = number of rows. Other parameters will be calculated automatically inside script


USEFUL LINKS:
---
**How to install OpenCV3.* for python 3.***
http://milq.github.io/install-opencv-ubuntu-debian/

