#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 17:41:22 2017

@author: max
"""

import os
import glob


path = "./together/"
#path_len = len(path)

labels = []

for f in sorted(glob.glob(path+"*.png")):
    
    f = f.lstrip(path+"num_")
    f = f[:-9]
    
    labels.append(f)
    
    print ("Current File LABEL is: " + f)
    
with open('labels.list', 'w') as file:
    for lb in labels:
        file.write(lb+"\n")