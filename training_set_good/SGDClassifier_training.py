#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 15:44:03 2017

@author: max
"""

import numpy as np
from sklearn.externals import joblib
from sklearn import datasets
from sklearn.linear_model import SGDClassifier
from skimage.feature import hog

###############
# load features
features = np.load("features_final.npy")
labels = []
with open("./labels.list") as f:
    for line in f.readlines():
        ln = line.rstrip("\n")
        if(ln=="0"):
            labels.append(" ")
        else:
            labels.append(str(ln))
        
labels = np.array(labels, np.str0)
        

# initialize and train classifier
mbkm = SGDClassifier()
mbkm.fit(features, labels)

# save trained classifier
joblib.dump(mbkm, "personal_sgd_32.pkl", compress=3)