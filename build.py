import sys
from cx_Freeze import setup, Executable

includes = []
excludes = []
packages = ['numpy', 'scipy', 'matplotlib', 'tkinter', 'cv2', 'argparse', 'os', 'glob', 'colorama', 'datetime', 'sklearn', 'skimage']
path = []
include_files = [
        ["./classifiers/", "./classifiers/"],
        ["./images/first.jpg", "./images/first.jpg"],
        ["./external_sudoku_solver.py", "./external_sudoku_solver.py"]
                ]

base = 'Console'

#2 = max optimization, (0,1,2)

options = {
    'build_exe': {
        "includes": includes,
        "excludes": excludes,
        "packages": packages,
        "path": path,
        "optimize": 2,
        "build_exe": "./dist/",
        "include_files": include_files
        #'excludes': ['Tkinter']  # Sometimes a little finetuning is needed
    }
}

executables = [Executable('main.py', base=base, targetName="grid_explorer.exe")]
setup(name='grid_explorer',
      version='2.0',
      description='Solve sudoku from photo!',
      executables=executables,
      options=options
      )